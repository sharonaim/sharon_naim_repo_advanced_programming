#include "Vector.h"

Vector::Vector(int n)
{
	if (n < 2){ // factor have to be at least size 2.
		n = 2;
	}

	this->_resizeFactor = n;
	this->_capacity = this->_resizeFactor;
	this->_elements = new int[this->_capacity];
	this->_size = 0; // empty
}

Vector::Vector(const Vector& other)
{
	*this = other;
}

Vector::~Vector()
{
	if (!this->_elements)
	{
		delete[] this->_elements;
		this->_elements = nullptr;
	}
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return !(this->_size);
}

void Vector::push_back(const int& val)
{
	this->_size++;
	if (this->_size >= this->_capacity + 1) { // If full.
		this->changeSize(1);
	}
	this->_elements[this->_size - 1] = val;
}

int Vector::pop_back()
{
	if (this->_size)
	{
		this->_size--;
		return this->_elements[this->_size];
	}
	else
	{
		std::cerr << ERR_EMPTY << std::endl;
		return ERR_VALUE;
	}
}

void Vector::reserve(int n)
{
	float times = n - this->_capacity;
	if (times > 0) // checks if there's something to allocate
	{
		int realTime = ceil(times / this->_resizeFactor); // so it will be int rounded but up. Ex: 1.2 -> 2.
		this->changeSize(realTime);
	}
}

void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		this->reserve(n);
	}
	else
	{
		this->_size = n;
		float times = n - this->_capacity;
		n = ceil(times / this->_resizeFactor); // times to multiple with resize factor
		this->changeSize(n);
	}
}

void Vector::resize(int n, const int& val)
{
	this->resize(n);

	for (int i = this->_size; i < this->_capacity; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}


void Vector::changeSize(int times)
{
	int* save = this->_elements;
	this->_capacity += this->_resizeFactor * times;
	this->_elements = new int[this->_capacity];
	if (this->_size > this->_capacity) {
		this->_size = this->_capacity;
	}
	
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = save[i];
	}

	delete[] save;
}

Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) {  // same object
		return *this;
	}
	
	delete[] this->_elements; // old vector

	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_elements = new int[this->_capacity];

	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

int& Vector::operator[](int n) const
{
	return this->_elements[n];
}

Vector Vector::operator+(const Vector& other) const
{
	Vector save = other;
	save += *this;
	return save;
}

Vector Vector::operator-(const Vector& other) const
{
	Vector save = other;
	save -= *this;
	return save;
}

Vector& Vector::operator+=(const Vector& other)
{
	for (int i = 0; i < this->_size && i < other._size; i++)
	{
		this->_elements[i] += other[i];
	}

	return *this;
}

Vector& Vector::operator-=(const Vector& other)
{
	for (int i = 0; i < this->_size && i < other._size; i++)
	{
		this->_elements[i] -= other[i];
	}

	return *this;
}