#include <iostream>
#include "Vector.h"

using std::cout;
using std::endl;

int main()
{
	Vector a(4);
	Vector b(4);
	
	for (int i = 0; i < 5; i++)
	{
		a.push_back(i);
		b.push_back(i);
	}

	Vector c = a + b;

	for (int i = 0; i < 5; i++)
	{
		cout << c[i] << endl;
	}

	system("pause");
	return 0;
}