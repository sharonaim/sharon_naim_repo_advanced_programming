#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
public:
	// C'tor and D'tor 
	String(std::string val);
	~String();

	// setters and getters
	void setValue(const std::string val);
	std::string getValue() const;

	virtual bool isPrintable() const;
	virtual std::string toString() const;

private:
	std::string _value;
};

#endif // STRING_H