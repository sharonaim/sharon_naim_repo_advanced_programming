#include "Void.h"

Void::Void() : Type()
{
}

Void::~Void()
{
}

bool Void::isPrintable() const
{
	return false; // void cannot be printed
}

std::string Void::toString() const
{
	return "Void";
}