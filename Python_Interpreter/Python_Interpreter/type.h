#pragma once
#include <string>
#include "Helper.h"

class Type
{
public:
	// C'tor and D'tor 
	Type();
	~Type();

	// setters and gettters
	void setTemp(const bool temp);
	bool getTemp() const;

	// virtual functions
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;

private:
	bool _isTemp;
};