#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
public:
	// C'tor and D'tor 
	Boolean(bool val);
	~Boolean();

	// setters and getters
	void setValue(const bool val);
	bool getValue() const;

	virtual bool isPrintable() const;
	virtual std::string toString() const;

private:
	bool _value;
};

#endif // BOOLEAN_H