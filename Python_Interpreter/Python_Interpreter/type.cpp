#include "type.h"


Type::Type()
{
	this->_isTemp = false; // default option.
}

Type::~Type()
{
}

void Type::setTemp(const bool temp)
{
	this->_isTemp = temp;
}

bool Type::getTemp() const
{
	return this->_isTemp;
}