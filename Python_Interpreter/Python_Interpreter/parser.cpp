#include "parser.h"
#include <iostream>

std::unordered_map<std::string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	Type* var = nullptr;

	if (str == QUIT_CMD || str == EXIT_CMD)
	{ // endline and will exit.
		std::cout << std::endl;
	}
	else if (str[FIRST] == TAB || isspace(str[FIRST])) 
	{ // checking if command starts with tab or space.
		throw(IndentationException());
	}
	else if(str.length() > 0)
	{
		Helper::rtrim(str); // removing spaces from the end of string.

		if (makeAssignment(str, true))
		{ // if a variable Assaignment
			var = new Void();
			var->setTemp(true);
		}
		else
		{
			var = getVariableValue(str, true);
			
			if (!var)
			{ // is still NULL.
				var = getType(str);
			}
		}
	}

	return var;
}

Type* Parser::getType(std::string &str)
{
	Type* variable = nullptr;

	if (Helper::isBoolean(str))
	{
		variable = new Boolean(str == "True" ? true : false); // turning string to right expression.
	}
	else if (Helper::isInteger(str))
	{
		variable = new Integer(std::stoi(str)); // turning the string to int.
	}
	else if (Helper::isString(str))
	{
		variable = new String(str);
	}
	else
	{
		throw (SyntaxException()); // none of our able variable options.
	}

	variable->setTemp(true); // it is temp.
	return variable;
}

bool Parser::isLegalVarName(const std::string& str, bool tryCheck)
{
	std::string save = str;
	if (Helper::isDigit(str[FIRST]))
	{ // first char cannot be a digit.
		if (tryCheck)
		{
			return false;
		}
		else
		{
			throw SyntaxException();
		}
	}
	
	for (std::string::iterator it = save.begin(); it != save.end(); ++it)
	{
		char chr = *it;
		if (!(Helper::isDigit(chr) || Helper::isLetter(chr) || Helper::isUnderscore(chr)))
		{ // if none of the above.
			if (tryCheck)
			{
				return false;
			}
			else
			{
				throw SyntaxException(); // an ilegal variable name.
			}
		}
	}

	return true; // legal.
}

bool Parser::makeAssignment(const std::string& str, bool tryCheck)
{ // TODO: edge case of when '=' is in a string variable.
	if (std::count(str.begin(), str.end(), '=') != 1) // only one is allowed.
	{
		return false;
	}

	int pos = str.find('=');

	std::string name_variable = str.substr(0, pos); // getting name.
	std::string value_variable = str.substr(pos + 1); // getting value.

	Helper::rtrim(name_variable);
	Helper::trim(value_variable); // cleaning them from whitespaces.

	Type* var = nullptr;
	bool legality_name = isLegalVarName(name_variable); // faster bacause we only do it once now.
	bool legality_variable = isLegalVarName(value_variable, true);


	
	if (legality_variable && legality_name)
	{
		if (Parser::_variables.find(value_variable) != Parser::_variables.end() && Parser::_variables[value_variable]) // if the name is in map and has variable.
		{
			value_variable = Parser::_variables[value_variable]->toString();
		}
		else
		{
			if (tryCheck)
			{
				return false;
			}
			else
			{
				throw NameErrorException(value_variable); // getting the string of the variable.
			}
		}
	}
	if (legality_name)
	{
		var = getType(value_variable);
		var->setTemp(false);
	}

	if (var) // not NULL.
	{
		if (Parser::_variables.find(name_variable) != Parser::_variables.end())
		{ // cleaning prevous variable.
			delete Parser::_variables[name_variable];
		}
		Parser::_variables[name_variable] = var; // adding to or changing variable in the map.
	}

	return true;
}

Type* Parser::getVariableValue(const std::string &str, bool tryCheck)
{
	if (Helper::isBoolean(str))
	{
		return NULL; // False or True.
	}
	else if (isLegalVarName(str))
	{
		if (Parser::_variables.find(str) != Parser::_variables.end())
		{ // if there's such variable.
			return Parser::_variables[str];
		}
		else
		{
			if (tryCheck)
			{
				return NULL;
			}
			else
			{
				throw NameErrorException(str);
			}
		}
	}
	else
	{
		return NULL; // an ilegal name.
	}
}

void Parser::freeVariables()
{
	for (std::unordered_map<std::string, Type*>::iterator it = Parser::_variables.begin(); it != Parser::_variables.end(); ++it)
	{
		delete Parser::_variables[it->first];
	}
}