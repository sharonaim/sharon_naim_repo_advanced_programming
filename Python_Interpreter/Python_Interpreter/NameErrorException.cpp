#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
	this->_name = "NameError : name '" + name + "' is not defined";
}

const char* NameErrorException::what() const throw()
{
	return this->_name.c_str();
}