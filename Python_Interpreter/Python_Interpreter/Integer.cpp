#include "Integer.h"

Integer::Integer(int val) : Type()
{
	this->_value = val;
}

Integer::~Integer()
{
}

void Integer::setValue(const int val)
{
	this->_value = val;
}

int Integer::getValue() const
{
	return this->_value;
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(this->_value);
}