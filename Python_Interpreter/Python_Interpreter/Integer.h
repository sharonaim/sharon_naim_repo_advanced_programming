#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
public:
	// C'tor and D'tor 
	Integer(int val);
	~Integer();

	// setters and getters
	void setValue(const int val);
	int getValue() const;

	virtual bool isPrintable() const;
	virtual std::string toString() const;
	
private:
	int _value;
};

#endif // INTEGER_H