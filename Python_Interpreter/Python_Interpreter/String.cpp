#include "String.h"

String::String(std::string val) : Sequence()
{
	val.erase(val.begin());
	val.erase(val.end() - 1, val.end()); // deleting the closers.
	this->_value = val;
}

String::~String()
{
}

void String::setValue(const std::string val)
{
	this->_value = val;
}

std::string String::getValue() const
{
	return this->_value;
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	std::string closer = "\'"; // default closers.

	if (this->_value.find('\'') != std::string::npos) // found ' char in string.
	{
		closer = "\""; // when the default is in the string itself.
	}

	return closer + this->_value + closer;
}