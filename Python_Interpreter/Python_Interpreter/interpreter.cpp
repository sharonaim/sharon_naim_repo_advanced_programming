#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define MY_NAME "SharoNaim"


int main(int argc,char **argv)
{
	std::cout << WELCOME << MY_NAME << std::endl;
	std::string input_string;
	Type* holder;
	
	do
	{
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);

		// trying to prase command
		try
		{
			holder = Parser::parseString(input_string);

			if (holder) // not NULL
			{
				if (holder->isPrintable()) // checking if printable
				{
					std::cout << holder->toString() << std::endl;
				}

				if (holder->getTemp())
				{
					delete holder;
				}
			}
		}
		catch (std::exception& exp)
		{
			std::cout << exp.what() << std::endl;
		}
		catch (...)
		{
			std::cout << UNKNOWN_EXCEPTION << std::endl;
		}
	} while (input_string != QUIT_CMD && input_string != EXIT_CMD);

	Parser::freeVariables();
	return 0;
}