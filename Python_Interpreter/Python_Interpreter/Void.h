#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
public:
	// C'tor and D'tor 
	Void();
	~Void();

	virtual bool isPrintable() const;
	virtual std::string toString() const;

private:
	// is void
};

#endif // VOID_H