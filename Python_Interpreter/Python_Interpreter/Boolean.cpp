#include "Boolean.h"

Boolean::Boolean(bool val) : Type()
{
	this->_value = val;
}

Boolean::~Boolean()
{
}

void Boolean::setValue(const bool val)
{
	this->_value = val;
}

bool Boolean::getValue() const
{
	return this->_value;
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return this->_value ? "True" : "False";
}