#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "type.h"
#include "Boolean.h"
#include "String.h"
#include "Integer.h"
#include "Void.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <algorithm>

#define FIRST 0
#define TAB '\t'
#define QUIT_CMD "quit()"
#define EXIT_CMD "exit()"
#define TRUE "True"
#define FALSE "False"

class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);
	static void freeVariables();

private:
	static bool isLegalVarName(const std::string& str, bool tryCheck = false);
	static bool makeAssignment(const std::string& str, bool tryCheck = false);
	static Type* getVariableValue(const std::string &str, bool tryCheck = false);

	static std::unordered_map<std::string, Type*> _variables;
};

#endif //PARSER_H