#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

#define LISTEN_PORT 8876

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(LISTEN_PORT);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}

	//system("pause");
	return 0;
}