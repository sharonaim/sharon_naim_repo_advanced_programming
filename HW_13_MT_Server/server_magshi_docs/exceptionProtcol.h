#pragma once
#include <exception>

#define UNKNOWN_ERR "An unkown error has occurred..."
#define ERR_PROTOCOL "Error in client protocol usage..."

class exceptionProtcol : public std::exception
{
	virtual const char* what() const
	{
		return ERR_PROTOCOL;
	}
};