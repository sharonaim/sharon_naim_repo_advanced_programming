#include "Server.h"

Server::Server()
{
	std::ifstream file(PATH);
	if (file.is_open())
	{ // takes all the file and puts it into a buffer, then copies it to data variable.
		std::stringstream buffer;
		buffer << file.rdbuf();
		this->_data = buffer.str();
		file.close();
	}
	else
	{ // if no file then the data variable is empty
		this->_data = "";
	}
	
	this->update_lock = unique_lock<mutex>(lock);
	this->_running = true;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (this->_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	this->_running = false;
	this->update_lock.release();
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(this->_serverSocket);
		for (map<string, SOCKET>::iterator it = this->_mapClients.begin(); it != this->_mapClients.end(); ++it)
		{
			closesocket(it->second);
		}
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	
	this->setup();
}

void Server::setup()
{
	std::thread threadUpdate(&Server::threadUpdater, this); // updating the data every 10 seconds.
	threadUpdate.detach();

	// no need to make it a thread, as it's the only thing we need to do in main now.
	this->accepter();
}

void Server::accepter()
{
	while (this->_running)
	{
		// this accepts the client and create a specific socket from server to this client
		std::cout << "Waiting for client connection request" << std::endl;
		SOCKET client_socket = accept(this->_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		// the function that handle the conversation with the client
		std::thread threadSocket(&Server::threadClient, this, client_socket);
		threadSocket.detach();
	}
}

void Server::threadUpdater()
{
	while (this->_running)
	{
		this->_updateWaiter.wait(update_lock);
		SOCKET holder;

		this->_queueLock.lock(); // getting the first and second users, 
		// if none then goes back to waiting.
		if (this->_clientsQueue.empty())
		{
			continue;
		}
		string first = this->_clientsQueue[FIRST];
		string second = this->_clientsQueue.size() > SECOND ? this->_clientsQueue[SECOND] : ""; // if there's no second client then empty string.
		this->_queueLock.unlock();

		for (int i = 0; i < this->_clientsQueue.size(); i++)
		{
			this->_mapLock.lock();
			holder = this->_mapClients[this->_clientsQueue[i]]; // getting the socket of client.
			this->_mapLock.unlock();

			Helper::sendUpdateMessageToClient(holder, this->_data, first, second, i + 1); // sending update to each client.
		}
	}
}

void Server::threadClient(SOCKET client)
{
	string name;
	bool client_connection = true;
	try
	{
		int type = Helper::getMessageTypeCode(client);
		if (type != MT_CLIENT_LOG_IN)
		{ // when client doesn't start with 200 msg.
			throw exceptionProtcol();
		}
		name = this->clientLogin(client);
		std::cout << "Client accepted, name - " << name << ". Server and client can speak" << std::endl;
		// got the name and replied him with a 101 msg.


		while (client_connection)
		{
			type = Helper::getMessageTypeCode(client); // getting code.

			switch (type)
			{
			case MT_CLIENT_UPDATE: // update to clients.
				this->update(client);
				break;
			case MT_CLIENT_FINISH: // finish writing.
				this->finish(client);
				break;
			case MT_CLIENT_EXIT: // exit.
				this->clientLogout(name);
				client_connection = false;
				break;
			default:
				client_connection = false; // something odd.
				break;
			}
		}
	}
	catch (std::exception& err)
	{
		std::cout << err.what() << std::endl;
	}
	catch (...)
	{ // for if client disconnects suddenly	
		std::cout << UNKNOWN_ERR << std::endl;
		this->clientLogout(name);
	}
	// Closing the socket (in the level of the TCP protocol)
}

void Server::update(SOCKET sc)
{
	int size = Helper::getIntPartFromSocket(sc, DATA_SIZE); // getting size of data.
	this->_data = Helper::getStringPartFromSocket(sc, size); // getting the data.

	std::ofstream ofs;
	ofs.open(PATH, std::ofstream::out | std::ofstream::trunc);
	ofs << this->_data; // writing the data that got from the writer client to file.
	ofs.close();

	this->_updateWaiter.notify_one(); // notifying updater.
}

void Server::finish(SOCKET sc)
{
	this->_queueLock.lock(); 
	string hold = this->_clientsQueue.front();
	this->_clientsQueue.pop_front(); // moving the first user to the back of the queue.
	this->_clientsQueue.push_back(hold);
	this->_queueLock.unlock();

	this->update(sc); // calling update.
}

void Server::clientLogout(string name)
{ // deleting the client from the queue and map.
	this->_mapLock.lock();
	closesocket(this->_mapClients[name]);
	this->_mapClients.erase(name);
	this->_mapLock.unlock();

	this->_queueLock.lock();
	for (deque<string>::iterator it = this->_clientsQueue.begin(); it != this->_clientsQueue.end(); ++it)
	{
		if (name == *it)
		{ // found the client name and now erasing from queue.
			this->_clientsQueue.erase(it);
			break;
		}
	}
	this->_queueLock.unlock();
	std::cout << name << " logged out succecfuly!" << std::endl;
	this->_updateWaiter.notify_one(); // notifying the updater.
}

string Server::clientLogin(SOCKET& sc)
{
	int size = Helper::getIntPartFromSocket(sc, NAME_SIZE);
	string name = Helper::getStringPartFromSocket(sc, size);
	
	// adding to map and queue.
	this->_mapLock.lock();
	this->_mapClients.insert(std::pair<string, SOCKET>(name, sc));
	this->_mapLock.unlock();

	this->_queueLock.lock();

	this->_clientsQueue.push_back(name);

	string first = this->_clientsQueue[FIRST]; // sending 101 msg to the new client.
	string second = this->_clientsQueue.size() > SECOND ? this->_clientsQueue[SECOND] : ""; 
	Helper::sendUpdateMessageToClient(sc, this->_data, first, second, this->_clientsQueue.size());
	
	this->_queueLock.unlock();

	return name;
}