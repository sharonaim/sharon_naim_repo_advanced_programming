#pragma once
#include "pch.h"

#define UPDATE_TIME 10
#define FIRST 0
#define SECOND 1
#define PATH "shared_doc.txt"
#define NAME_SIZE 2
#define DATA_SIZE 5


using std::string;
using std::vector;
using std::map;
using std::deque;
using std::condition_variable;
using std::mutex;
using std::unique_lock;


class Server
{
public:
	Server();
	~Server();

	void serve(int port);

	void setup();
	void accepter();
	void threadUpdater();
	void threadClient(SOCKET client);

	string clientLogin(SOCKET& sc);
	void clientLogout(string name);
	void update(SOCKET sc);
	void finish(SOCKET sc);

private:
	deque<string> _clientsQueue;
	map<string, SOCKET> _mapClients;
	condition_variable _updateWaiter;
	mutex _queueLock;
	mutex _mapLock;
	string _data;

	SOCKET _serverSocket;
	bool _running;
	int _currentEditor;
	mutex lock;
	unique_lock<mutex> update_lock;
};