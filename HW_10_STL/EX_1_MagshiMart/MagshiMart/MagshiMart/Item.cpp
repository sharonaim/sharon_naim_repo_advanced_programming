#include "Item.h"


Item::Item(string name, string number, double price)
{
	this->_name = name;
	this->_serialNumber = number;

	if (price <= 0)
	{
		throw PriceException();
	}
	this->_unitPrice = price;
	this->_count = 1;
}

Item::Item(const Item& other)
{
	this->_count = other._count;
	this->_name = other._name;
	this->_serialNumber = other._serialNumber;
	this->_unitPrice = other._unitPrice;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}

void Item::addCount()
{
	this->_count++;
}

void Item::subCount()
{
	this->_count--;
}

string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getPrice() const
{
	return this->_unitPrice;
}

void Item::setName(const string name)
{
	this->_name = name;
}

void Item::setSerialNumber(const string number)
{
	this->_serialNumber = number;
}

void Item::setCount(const int count)
{
	if (count <= 0)
	{
		throw CountException();
	}
	this->_count = count;
}

void Item::setPrice(const double price)
{
	if (price <= 0)
	{
		throw PriceException();
	}
	this->_unitPrice = price;
}

std::ostream& operator<<(std::ostream& stream, const Item& item)
{
	stream << item.getName() << " - count: " << item.getCount();
	return stream;
}