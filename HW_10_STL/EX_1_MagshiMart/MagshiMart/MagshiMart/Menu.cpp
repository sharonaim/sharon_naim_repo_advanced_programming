#include "Menu.h"

Item Menu::itemList[10] = {
			Item("Milk","00001",5.3),
			Item("Cookies","00002",12.6),
			Item("Bread","00003",8.9),
			Item("Chocolate","00004",7.0),
			Item("Cheese","00005",15.3),
			Item("Rice","00006",6.2),
			Item("Fish", "00008", 31.65),
			Item("Chicken","00007",25.99),
			Item("Cucumber","00009",1.21),
			Item("Tomato","00010",2.32) };

Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::openingScreen()
{
	try
	{
		int choice = 0;
	
		do {
			std::cout << "Welcome to MagshiMart!" << std::endl
				<< "1. to sign as customer and buy items" << std::endl
				<< "2. to update existing customer's items" << std::endl
				<< "3. to print the customer who pays the most" << std::endl
				<< "4. to exit" << std::endl;
			std::cin >> choice;
		} while (choice > EXIT || choice < SIGN_IN);
		this->clearScreen();

		switch (choice)
		{
		case SIGN_IN:
			this->newCustomer();
			break;
		case UPDATE:
			this->updateCustomer();
			break;
		case PRINT_MOST:
			this->paysMost();
			break;
		case EXIT:
			this->exitMenu();
			break;
		default:
			_exit(ERROR_EXIT);
			break;
		}
	}
	catch (std::exception& except)
	{
		std::cout << except.what() << std::endl;
		this->retOpening();
	}
	catch (...)
	{
		std::cout << UNKNOWN_ERR << std::endl;
	}
	this->retOpening();
}

void Menu::newCustomer()
{
	string name;
	
	std::cout << "Enter the customer's name: ";
	std::cin >> name;
	
	if (this->inMap(name))
	{
		throw NameException();
	}
	else
	{
		this->_customers.insert(std::pair<string, Customer>(name, Customer(name)));
		this->addItems(name);
	}
}

bool Menu::inMap(const string name)
{
	return this->_customers.find(name.c_str()) != this->_customers.end();
}

void Menu::updateCustomer()
{
	string name;

	std::cout << "Enter the customer's name: ";
	std::cin >> name;

	if (!this->inMap(name))
	{
		throw AccessException();
	}
	else
	{
		int choice = 0;

		do
		{
			std::cout << "1. Add items" << std::endl
				<< "2. Remove items" << std::endl
				<< "3. Back to menu" << std::endl;
			std::cin >> choice;
		} while (choice < ADD_TO || choice > BACK_MENU);

		switch (choice)
		{
		case ADD_TO:
			this->addItems(name);
			break;
		case REMOVE_ITEMS:
			this->removeItems(name);
			break;
		case BACK_MENU:
		default:
			break;
		}
	}
}

void Menu::paysMost()
{
	double most = 0;
	double sum = 0;
	map<string, Customer>::iterator save;

	if (!this->_customers.size())
	{
		throw EmptyException();
	}

	for (map<string, Customer>::iterator it = this->_customers.begin(); it != this->_customers.end(); ++it)
	{
		sum = it->second.totalSum();
		if (sum > most)
		{
			most = sum;
			save = it;
		}
	}

	std::cout << save->first << ", pays the most out of all with - " << most << std::endl;
	this->printOwn(save->first);
	this->retOpening();
}

void Menu::exitMenu()
{
	_exit(GOOD_EXIT);
}

void Menu::addItems(const string name)
{
	unsigned int choice = 0;

	this->clearScreen();
	do
	{
		try
		{
			std::cout << "The items you can buy are: (0 to exit)" << std::endl;
			this->printMenu();

			std::cout << "Enter choice - ";
			std::cin >> choice;

			this->addToCustomer(name, choice);
		}
		catch (std::exception& except)
		{
			std::cout << except.what() << std::endl;
		}
	} while (choice);
}

void Menu::addToCustomer(const string name, const unsigned int choice)
{
	if (choice >= MENU_SIZE)
	{
		this->clearScreen();
		throw FindException();
	}
	else if(choice)
	{
		this->clearScreen();
		this->_customers[name].addItem(this->itemList[choice - 1]);
	}
}

void Menu::removeItems(const string name)
{
	int choice = 0;

	this->clearScreen();
	do
	{
		try
		{
			std::cout << "Customer items: " << std::endl;
			this->printOwn(name);
			std::cout << "Enter number for item to remove(0 to stop) - ";
			std::cin >> choice;

			if (choice)
			{
				this->clearScreen();
				this->_customers[name].removeItem(this->itemList[choice - 1]);
			}
		}
		catch (std::exception& except)
		{
			std::cout << except.what() << std::endl;
		}
	} while (choice);
}

void Menu::printMenu()
{
	for (int i = 0; i < MENU_SIZE; i++)
	{
		std::cout << i + 1 << ". " << this->itemList[i].getName()
			 << " - price: " << this->itemList[i].getPrice() << std::endl;
	}
}

void Menu::printOwn(const string name)
{
	if (!this->inMap(name))
	{
		throw NameException();
	}
	else
	{
		std::cout << this->_customers[name];
	}
}

void Menu::clearScreen()
{
	system("cls");
}

void Menu::retOpening()
{
	system("pause");
	system("cls");
	this->openingScreen();
}