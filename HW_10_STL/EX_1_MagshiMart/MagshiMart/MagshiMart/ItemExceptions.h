#pragma once
#include <exception>

#define ERR_COUNT "Count must be above 0!"
#define ERR_PRICE "Price mush be above 0!"
#define ERR_NAME "That name is already in our customer list..."
#define ERR_NOT_FOUND "Wasn't able to find the desired item"
#define ERR_ACCESS "Such customer doesn't exist..."
#define ERR_CUSTOMER "Sorry but there no customers yet to compare"
#define UNKNOWN_ERR "An unkown error has occurred..."

class CountException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_COUNT;
	}
};

class PriceException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_PRICE;
	}
};

class NameException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_NAME;
	}
};

class FindException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_NOT_FOUND;
	}
};

class AccessException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_ACCESS;
	}
};


class EmptyException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_CUSTOMER;
	}
};