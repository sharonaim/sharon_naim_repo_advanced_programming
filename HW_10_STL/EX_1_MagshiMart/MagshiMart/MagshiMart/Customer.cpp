#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item& newItem)
{
	if (this->_items.count(newItem))
	{
		Item save(*(this->_items.find(newItem)));
		save.addCount();
		this->_items.erase(newItem);
		this->_items.insert(save);
	}
	else
	{
		this->_items.insert(newItem);
	}
}

void Customer::removeItem(Item& byeItem)
{
	if (!(this->_items.count(byeItem)))
	{
		throw FindException();
	}
	else if (this->_items.find(byeItem)->getCount() == STARTING_COUNT)
	{
		this->_items.erase(byeItem);
	}
	else
	{
		Item save(*(this->_items.find(byeItem)));
		save.subCount();
		this->_items.erase(byeItem);
		this->_items.insert(save);
	}
}

//get and set functions
void Customer::setCustomerName(const string name)
{
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		if (it->getName() == name)
		{
			Item save(*it);
			save.setName(name);
			this->_items.erase(it);
			this->_items.insert(save);
			break;
		}
	}
	if (it == this->_items.end())
	{
		throw FindException();
	}
}

std::set<Item> Customer::getItems() const
{
	return this->_items;
}

string Customer::getCustomerName() const
{
	return this->_name;
}

std::ostream& operator<<(std::ostream& stream, const Customer& customer)
{
	std::set<Item> userItems = customer.getItems();
	int count = 0;

	for (std::set<Item>::iterator it = userItems.begin(); it != userItems.end(); ++it)
	{
		count++;
		stream << count << ". " << *(it) << std::endl;
	}
	return stream;
}