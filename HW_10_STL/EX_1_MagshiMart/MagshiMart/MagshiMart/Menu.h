#include "Customer.h"
#include<map>
#pragma once

using std::map;

enum openings{SIGN_IN = 1, UPDATE, PRINT_MOST, EXIT};
enum exits{GOOD_EXIT, ERROR_EXIT};
enum items{ MILK, COOKIE, BREAD, CHOCO, CHEESE, RICE, FISH, CHICKEN, CUCU, TOMATO };
enum updates { ADD_TO = 1, REMOVE_ITEMS, BACK_MENU };

#define MENU_SIZE 10

class Menu
{
public:
	Menu();
	~Menu();

	void openingScreen();
	void newCustomer();
	void updateCustomer();
	void paysMost();
	void exitMenu();

	void addItems(const string name);
	void removeItems(const string name);

private:
	map<string, Customer> _customers;
	
	static Item itemList[10];
	static void clearScreen();

	void addToCustomer(const string name, const unsigned int choice);
	void retOpening();
	void printMenu();
	void printOwn(const string name);
	bool inMap(const string name);
};