#pragma once
#include"Item.h"
#include<set>

#define STARTING_COUNT 1

class Customer
{
public:
	Customer(string name = "unknown");
	~Customer();

	double totalSum() const;//returns the total sum for payment
	void addItem(Item&);//add item to the set
	void removeItem(Item&);//remove item from the set

	//get and set functions
	void setCustomerName(const string name);

	std::set<Item> getItems() const;
	string getCustomerName() const;

	friend std::ostream& operator<<(std::ostream& stream, const Customer& customer);

private:
	string _name;
	std::set<Item> _items;
};
