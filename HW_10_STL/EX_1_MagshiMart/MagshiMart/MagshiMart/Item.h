#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#include "ItemExceptions.h"

#define ERR_COUNT "Count must be above 0!"
#define ERR_PRICE "Price mush be above 0!"

using std::string;

class Item
{
public:
	Item(string name, string number, double price);
	Item(const Item& other);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	
	void addCount();
	void subCount();

	//get and set functions
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getPrice() const;

	void setName(const string name);
	void setSerialNumber(const string number);
	void setCount(const int count);
	void setPrice(const double price);

	friend std::ostream& operator<<(std::ostream& stream, const Item& item);

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!
};