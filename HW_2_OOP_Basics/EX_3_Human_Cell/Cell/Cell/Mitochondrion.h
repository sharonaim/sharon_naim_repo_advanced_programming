#pragma once
#include "Protein.h"

#define GLOCUSE_NEEDED 50
#define CHAIN_LENGTH 6

class Mitochondrion
{
public:
	// constructors & destructor
	Mitochondrion();
	
	// init - Sets fields to 0.
	void init();

	// setters
	void set_glucose(const unsigned int glocuse_units);

	// method
	/*
	Input: Protien reference.
	Output: void.
	if the protein is capable to be a receptor,
	then turns _has_glocuse_receptor to True.
	Else turns to false.
	if protein has --> ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, END.
	*/
	void insert_glucose_receptor(const Protein & protein);

	/*
	Input: void.
	Output: bool.
	If the Mito can produce ATP then True.
	Else False. (_glocuse_level equal or greater then 50 
	and _has_glocuse_receptor is True).
	*/
	bool produceATP() const;

private:
	// fields
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};

