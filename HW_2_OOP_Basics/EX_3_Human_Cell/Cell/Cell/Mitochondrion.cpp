﻿#include "Mitochondrion.h"

// constructors & destructor
Mitochondrion::Mitochondrion()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

// init
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

// setters
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

// method
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcid chain[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE };
	AminoAcidNode* aminoAcid = protein.get_first();
	this->_has_glocuse_receptor = true; // default true.
	for (int i = 0; i < CHAIN_LENGTH; i++)
	{
		if (aminoAcid->get_data() != chain[i]) // goes through all the array and checks each one to see if the same.
		{
			this->_has_glocuse_receptor = false;
			break;
		}
		aminoAcid = aminoAcid->get_next();
	} // Didn't check if end of chain beacause 
	// it can be a part of a DNA so it will not be the end maybe.
}

bool Mitochondrion::produceATP() const
{ 
	return this->_has_glocuse_receptor &&
		this->_glocuse_level >= GLOCUSE_NEEDED; // requirements to produe ATP
}
