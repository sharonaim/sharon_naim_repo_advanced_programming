#include "Nucleus.h"
#include <iostream>
#include <algorithm>

using std::cout;
using std::endl;

// constructors & destructor
Nucleus::Nucleus(const string DNA_strand)
{
	this->match_complementary(DNA_strand);
}

Nucleus::Nucleus()
{
	this->_DNA_strand = "";
	this->_complementary_DNA_strand = "";
}

Nucleus::~Nucleus()
{
}

// setters
void Nucleus::init(const string dna_sequence)
{
	this->match_complementary(dna_sequence);
}

// getters
string Nucleus::getDNA_strand() const
{
	return this->_DNA_strand;
}

string Nucleus::getComplementary_DNA_strand() const
{
	return this->_complementary_DNA_strand;
}

// method
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string RNA = "";
	if (gene.is_on_complementary_dna_strand()) // Which DNA strand is wanted.
	{
		RNA = this->_complementary_DNA_strand.substr(gene.get_start(), gene.get_end() + 1);
	}
	else
	{
		RNA = this->_DNA_strand.substr(gene.get_start(), gene.get_end() + 1);
	}

	std::replace(RNA.begin(), RNA.end(), 'T', 'U'); // Replaces all the 'T' to 'U' between the input Gene.

	return RNA;
}

string Nucleus::get_reversed_DNA_strand() const
{
	string reversed_DNA(this->_DNA_strand.rbegin(), this->_DNA_strand.rend()); // Put the DNA_strand in reverse in the string.
	return reversed_DNA;
}

unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	unsigned int count = 0;

	for (int i = this->_DNA_strand.find(codon); i != this->_DNA_strand.npos;
		i = this->_DNA_strand.find(codon, i + codon.size())) // finds everytime the next substring - codon until the end of string.
	{
		count++;
	}
	return count;
}

void Nucleus::match_complementary(const string DNA_strand)
{
	int size = 0;
	string save = "";
	bool flag = false;
	this->_DNA_strand = DNA_strand;
	size = this->_DNA_strand.size();
	for (int i = 0; i < size && !flag; i++) // creating the complementary DNA strand
	{
		switch (this->_DNA_strand[i]) // which Nucl to replace with.
		{
		case NUCL_G:
			save += COMP_G;
			break;
		case NUCL_C:
			save += COMP_C;
			break;
		case NUCL_T:
			save += COMP_T;
			break;
		case NUCL_A:
			save += COMP_A;
			break;
		default:
			flag = true; // DNA_ERR
			break;
		}
	}

	if (flag)
	{
		std::cerr << DNA_ERR << endl;
		_exit(1); // Exits program.
	}
	else
	{
		this->_complementary_DNA_strand = save;
	}
}