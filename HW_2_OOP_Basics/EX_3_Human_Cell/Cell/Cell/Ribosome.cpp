#include "Ribosome.h"

using std::string;

Protein* Ribosome::create_protein(string &RNA_transcript)
{
	Protein* aminoChain = new Protein;
	int size = RNA_transcript.size();
	AminoAcid amino = UNKNOWN;
	string code = "";
	aminoChain->init();
	
	for (int i = 0; i < size - 1; i += 3)
	{ // Checks each amino acid to see if exists.
		code = RNA_transcript.substr(i, 3); // Copies the amino acid.
		amino = get_amino_acid(code);
		if (amino != UNKNOWN)
		{
			aminoChain->add(amino);
		}
		else
		{ // Amino acid doesn't exist.
			aminoChain->clear();
			delete aminoChain;
			return nullptr; // False
		}
	}
	return aminoChain;
}
