#pragma once

class Gene
{
public:

	// constructors & destructor - creating and destroying the object.
	Gene();
	Gene(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	~Gene();

	// init
	/*
	Input: int - start, int - end, bool - complementary or not.
	Output: void.
	Setting all the fields in the object to the params.
	*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	// setters
	void set_start(const unsigned int start);
	void set_end(const unsigned int end);
	void setComplementary_dna_strand(const bool on_complementary_dna_strand);

	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	// operators

	// copy operator

	// method

private:

	// fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};