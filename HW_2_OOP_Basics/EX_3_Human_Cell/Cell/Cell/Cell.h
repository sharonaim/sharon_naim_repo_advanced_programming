#pragma once
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Ribosome.h"

#define PROTEIN_ERR "Protein ERR:: Wasn't able to create a protein."
class Cell
{
public:
	// init - Sets every field it can.
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);

	// method
	/*
	Input: void.
	Output: bool.
	Checks if Mito can produce energy,
	with the Gene that it has and Nucleus.
	If it can they sets _atp_units to 100.
	*/
	bool get_ATP();

private:
	// fields
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};

