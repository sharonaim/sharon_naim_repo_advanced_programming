#pragma once
#include <string>
#include "Gene.h"

using std::string;

#define COMP_G 'C'
#define COMP_C 'G'
#define COMP_T 'A'
#define COMP_A 'T'

#define NUCL_G int('G')
#define NUCL_C int('C')
#define NUCL_T int('T')
#define NUCL_A int('A')

#define DNA_ERR "DNA Error:: DNA strand can only contain => 'G', 'C', 'T', 'A'"

class Nucleus
{
public:
	// constructors & destructor
	Nucleus();
	Nucleus(const string DNA_strand);
	~Nucleus();

	// init
	/*
	Input: string dna_sequence.
	Output: void
	Sets the DNA with the new param.
	And creates the complementary_DNA also.
	*/
	void init(const string dna_sequence);

	// getters
	string getDNA_strand() const;
	string getComplementary_DNA_strand() const;

	// operators

	// copy operator

	// method
	/*
	Input: reference of Gene object.
	Output: string RNA.
	Takes the part of gene that is in the wanted DNA strand, 
	and turns it into an RNA.
	*/
	string get_RNA_transcript(const Gene& gene) const;

	/*
	Input: void.
	Output: string reverse DNA.
	Reverse the DNA_strand and returns it.
	*/
	string get_reversed_DNA_strand() const;

	/*
	Input: string codon.
	Output: int count.
	Counts how many times the codon appears in the DNA.
	*/
	unsigned int get_num_of_codon_appearances(const string& codon) const;

private:
	// fields
	string _DNA_strand;
	string _complementary_DNA_strand;

	// method
	void match_complementary(const string DNA_strand);
};