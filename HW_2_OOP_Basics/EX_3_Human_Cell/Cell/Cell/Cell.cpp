#include "Cell.h"

/*
Nucleus _nucleus;
Ribosome _ribosome;
Mitochondrion _mitochondrion;
Gene _glocus_receptor_gene;
unsigned int _atp_units;
*/

void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
}

bool Cell::get_ATP()
{
	bool ready = NULL;
	string RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* aminoChain = this->_ribosome.create_protein(RNA_transcript);
	if (!aminoChain) // checking if got the protein.
	{
		std::cerr << PROTEIN_ERR << std::endl; // ERROR!
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*aminoChain);
	this->_mitochondrion.set_glucose(50); // So would have energy to produce.

	ready = this->_mitochondrion.produceATP();
	if (ready) // If can produce ATP.
	{
		this->_atp_units = 100;
	}

	return ready;
}