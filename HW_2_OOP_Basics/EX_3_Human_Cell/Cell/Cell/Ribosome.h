#pragma once
#include "Protein.h"

#define UNKNOWN_TRUE nullptr;

class Ribosome
{
/*
Input: RNA string.
Output: Protein pointer.
Turns an RNA transcript to a protein - amino Acids chain.
*/
public:
	static Protein * create_protein(std::string &RNA_transcript);
};

