#include "Gene.h" 

// constructors & destructor
Gene::Gene()
{
	this->_start = 0;
	this->_end = 0;
	this->_on_complementary_dna_strand = 0;
}

Gene::Gene(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

Gene::~Gene()
{
}

// setters
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}
void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}

void Gene::setComplementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// getters
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}