#ifndef UTILS_H
#define UTILS_H

#include "stack.h"

/*
INPUT: Pointer to array and it's size.
OUTPUT: Void.
Gets an array and it's size,
reverses all the numbers of array.
Ex. 321 -> 123.
*/
void reverse(int* nums, unsigned int size);

/*
INPUT: Void.
OUTPUT: Pointer to int array.
Asks from user 10 numbers,
then put them into array in reverse order they were put.
*/
int* reverse10();

#endif // UTILS_H
