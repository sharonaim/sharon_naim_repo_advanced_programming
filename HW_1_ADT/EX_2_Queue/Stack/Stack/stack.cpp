#include "stack.h"

void push(stack* s, unsigned int element)
{
	insert(&(s->_element), element);
}

int pop(stack* s)
{
	if (s)
	{
		return remove(&(s->_element));
	}
	else
	{
		std::cout << "Need to create stack" << std::endl;
		return -1;
	}
}

void initStack(stack* s)
{
	s->_element = NULL;
}

void cleanStack(stack* s)
{
	if (s)
	{
		clear(s->_element);
		delete(s);
	}
	else
	{
		std::cout << "Stack already cleaned" << std::endl;
	}
}