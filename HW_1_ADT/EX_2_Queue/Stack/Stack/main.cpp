#include "utils.h"

int main()
{
	int* arr = NULL;

	arr = reverse10();

	std::cout << "reversed - ";
	for (int i = 0; i < 10; i++)
	{
		std::cout << arr[i] << " ";
	}

	delete[] arr;
	system("pause");
	return 0;
}