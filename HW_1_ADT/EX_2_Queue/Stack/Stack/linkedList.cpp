#include "linkedList.h"

void insert(node** head, unsigned int num)
{
	node* copy = (*head);

	if (copy == NULL)
	{
		*head = new node;
		(*head)->_num = num;
		(*head)->_next = NULL;
	}
	else if (copy->_next == NULL)
	{
		copy->_next = new node; // New node
		copy->_next->_num = num;
		copy->_next->_next = NULL;
	}
	else
	{
		insert(&(copy->_next), num);
	}
}

int remove(node** head, node* last)
{
	node* copy = (*head);

	if (copy == NULL) // List is empty
	{
		return -1;
	}
	else if (copy->_next == NULL && last == NULL)
	{
		int save = copy->_num;
		delete copy;
		*head = NULL;
		return save;
	}
	else if (copy->_next == NULL)
	{
		int save = copy->_num;
		last->_next = NULL;
		delete copy; // deleting the removed node
		return save;
	}
	return remove(&(*head)->_next, copy);
}

node* last(node* head)
{
	if (head == NULL)
	{
		return NULL; // List is empty
	}
	else if (head->_next == NULL)
	{
		return head; // Last node in list.
	}
	return last(head->_next);
}

void print(node* head)
{
	if (head != NULL)
	{
		std::cout << " - " << head->_num << std::endl;
		print(head->_next);
	}
}

void clear(node* head)
{
	if (head != NULL)
	{
		clear(head->_next);
		delete head;
	}
}