#include "utils.h"

void reverse(int* nums, unsigned int size)
{
	stack* save = new stack;
	initStack(save);
	
	for (unsigned int i = 0; i < size; i++)
	{
		push(save, nums[i]);
	}

	for (unsigned int i = 0; i < size; i++)
	{
		nums[i] = pop(save);
	}
	
	cleanStack(save);
}

int* reverse10()
{
	int* newArr = new int[10];

	for (int i = 0; i < 10; i++)
	{
		std::cout << "Enter value of number " << i + 1 << ":";
		std::cin >> newArr[i];
	}
	reverse(newArr, 10);

	return newArr;
}