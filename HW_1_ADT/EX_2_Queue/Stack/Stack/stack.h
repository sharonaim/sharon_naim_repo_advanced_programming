#ifndef STACK_H
#define STACK_H

#include "linkedList.h"
/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	node* _element;
} stack;

/*
INPUT: The stack and element.
OUTPUT: Void.
Pushes the element into the stack.
*/
void push(stack* s, unsigned int element);
/*
INPUT: The stack.
OUTPUT: Void.
Pop the last element that was pushed and returns the value of it.
*/
int pop(stack* s); // Return -1 if stack is empty

/*
INPUT: The stack.
OUTPUT: Void.
Creates the stack.
*/
void initStack(stack* s);
/*
INPUT: The stack.
OUTPUT: Void.
Deletes the stack.
*/
void cleanStack(stack* s);

#endif // STACK_H