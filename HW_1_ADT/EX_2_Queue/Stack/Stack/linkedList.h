#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>

typedef struct node
{
	int _num;
	node* _next;
} node;

/*
	INPUT: Address of head and number.
	OUTPUT: Void.
if theres no head yet then makes a new head.
else it adds the number to the end of the linkedList.
*/
void insert(node** head, unsigned int num);
/*
	INPUT: Address of head and last head for later recursive.
	OUTPUT: The number the was removed.
Checks first if theres a head.
Then if it's the end of linkedList.
If it is then removes it and return the value of it.
*/
int remove(node** head, node* last = NULL);
/*
	INPUT: The head of linkedList.
	OUTPUT: Node.
Just returns the last node.
*/
node* last(node* head);
/*
	INPUT: The head of linkedList.
	OUTPUT: Void.
Goes through all the linkedList and prints it in order of first to last.
*/
void print(node* head);
/*
	INPUT: The head of linkedList.
	OUTPUT: Void.
Deletes all the nodes of the linkedList.
*/
void clear(node* head);

#endif // LINKEDLIST_H