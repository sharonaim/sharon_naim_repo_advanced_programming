#include "queue.h"

int choose();
// Realised just in the end that I didn't need to send the main 
// but it's in my project in the local repo so can't remove it.
int main()
{
	queue* myQueue = new queue;
	int sizeQueue = 0;
	int choice = 0;
	int num = 0;

	std::cout << "Please enter size of queue: ";
	std::cin >> sizeQueue;

	initQueue(myQueue, sizeQueue); // Creating and cleaning the queue.
	cleanQueue(myQueue);

	while (choice != 4)
	{
		choice = choose();
		switch (choice)
		{
		case 1:
			std::cout << "Enter number to enqueue: " << std::endl;
			std::cin >> num;
			enqueue(myQueue, num);
			break;
		case 2:
			std::cout << "Number: " << dequeue(myQueue) << " , was dequeued" << std::endl;
			break;
		case 3:
			cleanQueue(myQueue);
			std::cout << "Queue was reset." << std::endl;
			break;
		default:
			break;
		}
	}
	
	std::cout << "See you next time! :)" << std::endl;
	delQueue(myQueue); // Deletes the queue.
	system("pause");
	return 0;
}

/*
INPUT: Void.
OUTPUT: The choice.
	Displays the user's options
	and gets the choice of the user.
*/
int choose()
{
	int choice = 0;
	
	while (1 > choice || choice > 4)
	{
		std::cout << "1 - Add another number to the queue." << std::endl;
		std::cout << "2 - Take out the first in line." << std::endl;
		std::cout << "3 - Reset the whole queue." << std::endl;
		std::cout << "4 - Close queue." << std::endl;
		std::cin >> choice;
		std::cout << std::endl;
	}

	return choice;
}