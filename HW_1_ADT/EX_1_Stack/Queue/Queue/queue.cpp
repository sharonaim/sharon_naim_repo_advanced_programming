#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->_queue = new int[size];
	q->_count = 0;
	q->_size = size;
}

void cleanQueue(queue* q)
{
	for (int i = 0; i < q->_size; i++)
	{
		q->_queue[i] = 0;
	}
	q->_count = 0;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->_count == q->_size) // Queue is full.
	{
		std::cout << "Sorry but the queue is full right now, \nplease wait until there's be a free spot..." << std::endl;
	}
	else
	{
		q->_queue[q->_count] = newValue;
		q->_count++;
	}
}

int dequeue(queue* q)
{
	int out = 0;
	if (q->_count == 0) // Checks if empty.
	{
		std::cout << "Queue is empty..." << std::endl;
		out = -1;
	}
	else
	{
		out = q->_queue[0];
		for (int i = 1; i < q->_count; i++)
		{
			q->_queue[i - 1] = q->_queue[i];
		}
		q->_count--;
		q->_queue[q->_count] = 0;
	}
	return out;
}

void delQueue(queue* q)
{
	delete[] q->_queue;
	delete q;
}