#ifndef QUEUE_H
#define QUEUE_H
#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	int* _queue;
	int _size;
	int _count;

} queue;

/*
	INPUT: Queue struct and size of wanted queue.
	OUTPUT: Void.
	creates the queue with the wanted size,
	saves the size of queue and reset the count.
*/
void initQueue(queue* q, unsigned int size);

/*
INPUT: Queue struct.
OUTPUT: Void.
	resets all the numbers in queue to 0,
	reset the count to 0 as well
	(doesn't do anything with size).
*/
void cleanQueue(queue* q);

/*
INPUT: Queue struct and number to enqueue.
OUTPUT: Void.
	If queue isn't full then adds the number
	to the last in queue and adds 1 to count.
*/
void enqueue(queue* q, unsigned int newValue);

/*
INPUT: Queue struct.
OUTPUT: Dequeued number.
	Checks if queue isn't empty.
	If it is then prints it to user,
	else it dequeues the first in line
	and subtracts 1 from count.
*/

int dequeue(queue* q); // return element in top of queue, or -1 if empty

/*
INPUT: Queue struct.
OUTPUT: Void.
	Just deletes the queue and the struct itself.
*/
void delQueue(queue* q);
#endif /* QUEUE_H */