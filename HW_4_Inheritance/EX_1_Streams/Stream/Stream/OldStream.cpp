#include "OldStream.h"

OldStream::OldStream()
{
}

OldStream::~OldStream()
{
}

OldStream& OldStream::operator<<(const char *str)
{
	printf("%s", str);
	return *this;
}

OldStream& OldStream::operator<<(int num)
{
	printf("%d", num);
	return *this;
}

OldStream& OldStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}

void endline()
{
	printf("\n");
}