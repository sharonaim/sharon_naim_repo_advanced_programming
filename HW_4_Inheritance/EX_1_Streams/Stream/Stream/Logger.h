#pragma once
#include "OldStream.h"
#include <stdio.h>

class Logger
{
private:
	OldStream os;
	bool _startLine;
	void setStartLine();
public:
	Logger(); // creates logger
	~Logger(); // deletes logger
	friend Logger& operator<<(Logger& l, const char *msg); // prints to logger the string with number of line
	friend Logger& operator<<(Logger& l, int num); // prints to logger the num with number of line
	friend Logger& operator<<(Logger& l, void(*pf)()); // newline with number of line
};