#include "OutStreamEncrypted.h"

OutStreamEncrypted::OutStreamEncrypted(int key) : OutStream()
{
	this->_key = key;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	char* save = new char[strlen(str) + 1];
	strcpy(save, str);

	for (int i = 0; save[i]; i++)
	{
		if (START_LETTER <= int(str[i]) && END_LETTER >= int(str[i]))
		{
			save[i] = char(int(save[i] + this->_key - START_LETTER) % LEN_LETTERS + START_LETTER);
		}
		else if(START_CAPS <= int(str[i]) && END_CAPS >= int(str[i]))
		{
			save[i] = char(int(save[i] + this->_key - START_CAPS) % LEN_LETTERS + START_CAPS);
		}
	}

	OutStream::operator<<(save);
	delete save;
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int enc_num = 0;
	int dig = 0;

	while (num)
	{
		int dig = num % NUM_DIGS;
		num /= NUM_DIGS;

		enc_num *= NUM_DIGS;
		enc_num += (dig + this->_key) % NUM_DIGS;
	}

	OutStream::operator<<(enc_num);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE*))
{
	OutStream::operator<<(pf);
	return *this;
}