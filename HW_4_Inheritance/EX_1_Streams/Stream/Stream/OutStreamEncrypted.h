#pragma once
#include "OutStream.h"
#include <string.h>

#define LEN_LETTERS 26
#define START_LETTER 97
#define END_LETTER 122
#define START_CAPS 65
#define END_CAPS 90
#define NUM_DIGS 10

class OutStreamEncrypted : public OutStream
{
public:
	OutStreamEncrypted(int key); // creates object
	~OutStreamEncrypted(); // deletes object

	OutStreamEncrypted& operator<<(const char *str); // prints the string, encrypted.
	OutStreamEncrypted& operator<<(int num); // prints the number, encrypted.
	OutStreamEncrypted& operator<<(void(*pf)(FILE*)); // newline.

private:
	int _key;
};