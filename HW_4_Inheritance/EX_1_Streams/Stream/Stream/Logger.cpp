#include "Logger.h"

Logger::Logger() : os(OldStream())
{
	this->_startLine = true;
}

Logger::~Logger()
{
}

Logger& operator<<(Logger& l, const char *msg)
{
	l.setStartLine();
	l.os << msg;
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	l.setStartLine();
	l.os << num;
	return l;
}

Logger& operator<<(Logger& l, void(*pf)())
{
	l.setStartLine();
	l.os << pf;
	l._startLine = true;
	return l;
}

void Logger::setStartLine()
{
	if (this->_startLine)
	{
		static int line_count = 0;
		this->_startLine = false;
		this->os << line_count << " LOG ";
		line_count++;
	}
}