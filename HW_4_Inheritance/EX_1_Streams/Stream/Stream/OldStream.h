#pragma once
#include <stdio.h>
#include <string>

class OldStream
{
public:
	OldStream(); // creates object.
	~OldStream(); // deletes object.

	OldStream& operator<<(const char *str); // prints the string.
	OldStream& operator<<(int num); // prints the number.
	OldStream& operator<<(void(*pf)()); // newline.
};

void endline();