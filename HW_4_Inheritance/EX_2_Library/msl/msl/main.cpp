#pragma comment(lib, "../Debug/msl.lib")
#include "OutStream.h"
#include "FileStream.h"

#define _CRT_SECURE_NO_WARNINGS

int main()
{
	msl::OutStream os;
	std::string path = "../fileStream.txt";
	msl::FileStream fileos(path);

	for (int i = 1; i < 6; i++)
	{
		os << i << " -> ";
		fileos << i << " -> ";
	}
	os << msl::endline << "I am " << 13 << " years old!";
	fileos << msl::endline << "I am " << 13 << " years old!";

	getchar();
	return 0;
}