#include "FileStream.h"

namespace msl
{
	FileStream::FileStream(std::string s)
	{
		this->_file = fopen(s.c_str(), "w");
	}


	FileStream::~FileStream()
	{
		fclose(this->_file);
	}
}