#pragma once
#include <stdio.h>
#include <string>

namespace msl
{
	class OutStream
	{
	protected:
		FILE * _file;

	public:
		OutStream(); // creates object.
		~OutStream(); // deletes object.

		OutStream& operator<<(const char *str); // prints the string.
		OutStream& operator<<(int num); // prints the number.
		OutStream& operator<<(void(*pf)(FILE*)); // newline.
	};

	void endline(FILE* f);
}