#pragma once
#include "OutStream.h"

namespace msl
{
	class FileStream : public OutStream
	{
	public:
		FileStream(std::string s); // creates the path to file.
		~FileStream(); // closes access to file.
	};
}