#include "OutStream.h"

namespace msl
{
	OutStream::OutStream()
	{
		this->_file = stdout;
	}

	OutStream::~OutStream()
	{
	}

	OutStream& OutStream::operator<<(const char *str)
	{
		fprintf(this->_file, "%s", str);
		return *this;
	}

	OutStream& OutStream::operator<<(int num)
	{
		fprintf(this->_file, "%d", num);
		return *this;
	}

	OutStream& OutStream::operator<<(void(*pf)(FILE*))
	{
		pf(this->_file);
		return *this;
	}

	void endline(FILE* f)
	{
		fprintf(f, "\n");
	}
}