#include "mySql.h"

mySql::mySql(string path)
{
	srand(time(NULL)); // init rand seed
	this->open(path); // opens sql database.
}

mySql::mySql()
{ // does nothing!
	srand(time(NULL)); // init rand seed
}

mySql::~mySql()
{
	this->close(); // closes sql database.
}

void mySql::open(string path)
{
	int fileExists = _access(path.c_str(), 0); // File status => 0 if exists, 1 if it doesn't.
	// for next when we are adding the tables.
	int ans = sqlite3_open(path.c_str(), &this->_database); 
	if (ans != SQLITE_OK)
	{
		throw OpenException();
	}

	if (fileExists) // Goes in if file didn't exist.
	{
		// init database
		this->init();
	}
}

void mySql::close()
{
	int ans = sqlite3_close(this->_database);
	if (ans != SQLITE_OK)
	{
		throw CloseException();
	}

	this->_database = nullptr;
}

void mySql::init()
{
	int ans = SQLITE_OK;
	char** errMsg = nullptr;

	string persons = R"(CREATE TABLE IF NOT EXISTS Persons(
							ID INTEGER PRIMARY KEY AUTOINCREMENT, 
							Last_Name TEXT NOT NULL,
							First_Name TEXT NOT NULL,
							Email TEXT NOT NULL
							);)";

	string Phone_Prefix = R"(CREATE TABLE IF NOT EXISTS Phone_Prefix(
								ID INTEGER PRIMARY KEY AUTOINCREMENT,
								Prefix TEXT NOT NULL
								);)";

	string phone = R"(CREATE TABLE IF NOT EXISTS Phone(
						  ID INTEGER PRIMARY KEY AUTOINCREMENT,
						  Phone_Number TEXT NOT NULL,
						  Phone_Prefix_ID INTEGER NOT NULL,
						  Person_ID INTEGER NOT NULL,
						  FOREIGN KEY (Phone_Prefix_ID) REFERENCES Phone_Prefix(ID),
						  FOREIGN KEY (Person_ID) REFERENCES Persons(ID)
						  );)";

	// init of all tables.
	ans += sqlite3_exec(this->_database, persons.c_str(), nullptr, nullptr, errMsg);
	ans += sqlite3_exec(this->_database, Phone_Prefix.c_str(), nullptr, nullptr, errMsg);
	ans += sqlite3_exec(this->_database, phone.c_str(), nullptr, nullptr, errMsg);

	if (ans != SQLITE_OK)
	{ // if all went right then ans variable wouldn't get any big, but stay the same.
		throw InitException();
	}
	this->insert_info(); // inserting info like prefix numbers and such.
}

void mySql::insert_info()
{
	this->insert_phone_prefix();
	this->insert_person();
	this->insert_numbers();
}

void mySql::insert_phone_prefix()
{
	std::vector<string> prefix_vec = {"02","03","04","08",
										"09","050","052","053",
									  "054","055","073","077"};
	int ans = SQLITE_OK;
	char** errMsg = nullptr;

	for(int i = 0; i < prefix_vec.size(); i++)
	{
		string insert_str = "INSERT INTO Phone_Prefix (prefix) VALUES ('" + prefix_vec[i] + "');";
		//std::cout << insert_str << std::endl;
		ans = sqlite3_exec(this->_database, insert_str.c_str(), nullptr, nullptr, errMsg);

		if (ans != SQLITE_OK)
		{
			throw InsertException();
		}
	}
}

void mySql::insert_person()
{
	int ans = SQLITE_OK;
	char** errMsg = nullptr;

	for (int i = 0; i < PERSON_SIZE; i++)
	{
		std::vector<string> vector_person = this->person_details(i);
		std::stringstream strStream;

		strStream << "INSERT INTO Persons\n"
			<< "(first_name, last_name, email)\n"
			<< "VALUES ('" << vector_person[PERSON_FIRST] << "', '"
			<< vector_person[PERSON_LAST] << "', '" << vector_person[PERSON_EMAIL] << "');";
		string insert_str = strStream.str();
		// std::cout << insert_str << std::endl;

		ans = sqlite3_exec(this->_database, insert_str.c_str(), nullptr, nullptr, errMsg);
		if (ans != SQLITE_OK)
		{
			throw InsertException();
		}
	}
}

std::vector<string> mySql::person_details(int number)
{
	string person_first;
	string person_last;
	string person_email;
	std::vector<string> v;

	std::cout << "Person " << number + 1 << " - " << std::endl;
	std::cout << "Enter first name: ";
	std::cin >> person_first;
	std::cout << "Enter last name: ";
	std::cin >> person_last;
	do
	{
		std::cout << "Enter email: ";
		std::cin >> person_email;
	} while (person_email.find('@') == NOT_FOUND);

	v.push_back(person_first);
	v.push_back(person_last);
	v.push_back(person_email);

	return v;
}

void mySql::insert_numbers()
{
	for (int i = 0; i < 3; i++)
	{
		this->insert_phone_number(this->generate_random_number(), 1, i);
	}
	for (int i = 0; i < 2; i++)
	{
		this->insert_phone_number(this->generate_random_number(), 2, i);
	}
	this->insert_phone_number(this->generate_random_number(), 3, 3);
}

void mySql::insert_phone_number(string number, int id_user, int id_prefix)
{
	int ans = SQLITE_OK;
	char** errMsg = nullptr;
	std::stringstream strStream;

	strStream << "INSERT INTO Phone\n"
		<< "(Phone_Number, Person_ID, Phone_Prefix_ID)\n"
		<< "VALUES ('" << number << "', '"
		<< id_user << "', '" << id_prefix << "');";
	string insert_str = strStream.str();
	// std::cout << insert_str << std::endl;

	ans = sqlite3_exec(this->_database, insert_str.c_str(), nullptr, nullptr, errMsg);
	if (ans != SQLITE_OK)
	{
		throw InsertException();
	}
}

string mySql::generate_random_number()
{
	string number;
	for (int i = 0; i < DIGIT_SIZE; i++)
	{
		number += std::to_string(rand() % DIGIT);
	}
	return number;
}