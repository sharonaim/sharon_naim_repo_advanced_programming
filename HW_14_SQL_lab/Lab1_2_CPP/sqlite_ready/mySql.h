#pragma once
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <io.h>
#include<time.h>

#include "sqlite3.h"
#include "SqlException.h"

#define PERSON_SIZE 3
#define NOT_FOUND -1
#define DIGIT_SIZE 7
#define DIGIT 10

enum details
{
	PERSON_FIRST,
	PERSON_LAST,
	PERSON_EMAIL
};

using std::string;

class mySql
{
public:
	// Constructors and Destructor
	mySql(string path);
	mySql(); // for if the user doesn't want to init right away.
	~mySql();

	// Tools
	void open(string path); // Open database.
	void close(); // Close database.

	// Init
	void init(); // Initialization of tables in database.
	void insert_info();

	// Helper functions
	std::vector<string> person_details(int number);

private:
	sqlite3* _database;

	void insert_person();
	void insert_phone_prefix();
	void insert_numbers();
	void insert_phone_number(string number, int id_user, int id_prefix);

	string generate_random_number();
};