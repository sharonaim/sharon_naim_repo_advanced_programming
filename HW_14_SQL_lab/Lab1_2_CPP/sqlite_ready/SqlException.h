#pragma once
#include <exception>

#define UNKNOWN_ERROR "An unknown error has occurred! Please fix me."

/*
	didn't use defines this time here,
	because actually think it's better
	for the reader like that.
*/

class OpenException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR00: Open - Failed to open a database. Please check if path is valid.";
	}
};

class CloseException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR01: Close - Failed to close the database.";
	}
};

class InitException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR02: Init - Failed to with the initialization of database.";
	}
};

class AccessException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR03: Access - Failed to access the database table.";
	}
};

class InsertException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "ERROR04: Insert - Failed to insert the data to table.";
	}
};