#include <iostream>

#include "sqlite3.h"
#include "mySql.h"

int main()
{
	std::string path = "MyDB.sqlite";
	mySql db;

	try
	{
		db.open(path);
		std::cout << "Success!!!" << std::endl;
	}
	catch (std::exception& exc)
	{
		std::cout << exc.what() << std::endl;
	}
	catch (...)
	{
		std::cout << UNKNOWN_ERROR << std::endl;
	}


	system("pause");
	return 0;
}