#pragma once
#include <iostream>
#define PI 3.14159265359

struct Shape
{
	virtual int getArea() const = 0;
};

struct Circle : public Shape
{
	int x;
	int y;
	int radius; 
	int getArea() const
	{
		return PI * std::pow(this->radius, 2);
	}
};

struct Rectangle : public Shape
{
	int x1;
	int y1;
	int x2;
	int y2;
	int getArea() const 
	{ 
		return (this->x2 - this->x1) * (this->y2 - this->y1);
	}
};