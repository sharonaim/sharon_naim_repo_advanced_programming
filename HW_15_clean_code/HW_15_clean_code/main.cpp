#include "Digit.h"
#include "SingleResponsibility.h"
#include <iostream>
#include <vector>


class Point
{
public:
	Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
	double distance(const Point& other) const
	{
		// now less repeatative
		return sqrt(pow(this->x - other.x, 2) + pow(this->y - other.y, 2));
	}

private:
	int x;
	int y;
};

bool isNum(std::string str);
std::string dayOfWeek(int day);
double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C);
double calcPentagonPerimeter(const Point& A, const Point& B, const Point& C, const Point& D, const Point& E);
bool validateEmail(const std::string& email);
float avg_vector(const std::vector<int>& value_vec);

int main()
{
	try
	{
		// **1**
		//std::string str = "12345";
		//std::cout << isNum(str) << std::endl;

		// **2**
		//std::cout << dayOfWeek(2) << std::endl;

		// **3**
		/*Point a(1, 1);
		Point b(2, 1);
		Point c(2, 2);
		Point d(1, 2);
		Point e(3, 3);
		std::cout << calcTrianglePerimeter(a, b, c) << std::endl;
		std::cout << calcPentagonPerimeter(a, b, c, d, e) << std::endl;*/

		//**4**
		// the checking of if the email is correct or not shouldn't be in Person class!
		/*std::string email = "sharon@naim.com";
		if (validateEmail(email))
		{
			Person("sharon", "naim", email);
		}
		else
		{
			throw std::invalid_argument("email address not valid");
		}*/

		//**5**
		// 1. what if we have 100 shapes?
		// 2. unsafe and hard to read.
		// 3. can just do a virtual area function.
		// 4. the calc of rectangle area is incorrect.

		//**6**
		// 1. it is correct.
		// 4. a furniture class and has TrySit, but we have a lamp that you can't sit on!

		//**7**
		/*
		struct IPhone
		{
		virtual void call() const = 0;
		virtual void sms() const {};
		virtual void fax() const {};
		};
		*/

		//**8**
		// 1. if will need to use a different way then email, will be really hard.
		// 3. not easy. need to add every single sending way.

		//**9**
		// 1. check if name is at certian length and if first letter is alpha,
		// and every char after that should be digit or alpha.
		// 2. hard to read and hard to change.

		//**10**
		// count the average of the vector's variables' value.

	}
	catch (std::exception& exc)
	{
		std::cout << exc.what() << std::endl;
	}
	catch (...)
	{
	}

	system("pause");
	return 0;
}

bool isNum(std::string str)
{
	// 4. but no need for that. because there's a function called isAlpha!!!
	if (str[0] == '0')
	{
		return false;
	}
	for (int i = 0; i < str.size(); i++)
	{
		Digit dig(str[i]);
	}
	return true;
}

std::string dayOfWeek(int day)
{
	// days will never change there number of quantity.
	switch (day)
	{
	case 1:
		return "Sunday";
	case 2:
		return "Monday";
	case 3:
		return "Tuesday";
	case 4:
		return "Wednesday";
	case 5:
		return "Thursday";
	case 6:
		return "Friday";
	case 7:
		return "Saturday";
	default:
		return "No such day.";
	}
}

double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C)
{
	// had the calculating mixed up.
	return A.distance(B) + B.distance(C) + C.distance(A);
}

double calcPentagonPerimeter(const Point& A, const Point& B, const Point& C, const Point& D, const Point& E)
{
	return A.distance(B) + B.distance(C) + C.distance(D) + D.distance(E) + E.distance(A);
}

bool validateEmail(const std::string& email)
{
	if (email.find('@') == std::string::npos)
		return false;
	return true;
}

float avg_vector(const std::vector<int>& value_vec)
{
	if (value_vec.empty())
		throw std::exception("invalid");
	float sum = 0.0;
	for (std::vector<int>::const_iterator it = value_vec.begin(), eend = value_vec.end(); it != end; ++it)
		sum += *it;
	return sum / value_vec.size();
}