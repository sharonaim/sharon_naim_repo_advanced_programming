#pragma once
#include <iostream>

class Instrument
{
public:
	virtual void playChord() const = 0;
};
class Guitar : public Instrument
{
public:
	void playChord() const
	{
		std::cout << "Am" << std::endl;
	}
};
class Drum : public Instrument
{
	void playChord() const
	{
		// We throw exception here because drum can't play chords
		std::cout << "boom bam (cannot play a chord)" << std::endl;
	}
};
void Musician(const Instrument& instrument)
{
	instrument.playChord();
}