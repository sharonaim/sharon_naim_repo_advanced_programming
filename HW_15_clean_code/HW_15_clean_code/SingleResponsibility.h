#pragma once
#include <string>

class Person
{
public:
	Person(const std::string& firstName, const std::string& lastName, const std::string& email)
		: _firstName(firstName), _lastName(lastName)
	{
		_email = email;
	}

private:
	std::string _firstName;
	std::string _lastName;
	std::string _email;

};