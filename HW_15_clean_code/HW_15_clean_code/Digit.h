#pragma once
#include <string>
#include "MyException.h"

class Digit
{
public:
	Digit(char num);
	~Digit();

private:
	int _number;
};

