#pragma once
#include <string>

class Device
{
public:
	Device()
	{
	}

	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	virtual void send() const = 0;
};

class Email : public Device
{
public:
	Email() : Device()
	{
	}

	void send() const
	{
		// Send email
	}
};

class SMS : public Device
{
public:
	SMS() : Device()
	{
	}

	void send() const
	{
		// Send SMS
	}
};

class Reminder
{
	Device *_pEmail;
	Device *_pSMS;
public:
	Reminder(const std::string& to,
		const std::string& from,
		const std::string& subject,
		const std::string& content) : _pEmail(nullptr), _pSMS(nullptr)
	{
		_pEmail = new Email();
		_pSMS = new SMS();
		_pEmail->to(to);
		_pSMS->to(to);
	}
	~Reminder()
	{
		delete _pEmail;
		delete _pSMS;
	}
	void sendReminder() const
	{
		_pEmail->send();
		_pSMS->send();
	}
};