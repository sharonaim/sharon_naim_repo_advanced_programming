#include "Digit.h"



Digit::Digit(char num)
{
	if (!(num >= '0' && num <= '9')) // is a digit.
	{
		throw MyException("Error : tried to input a non digit string.");
	}
	this->_number = std::atoi(&num);
}


Digit::~Digit()
{
}