#pragma once
#include <string>
#include <algorithm>

#define MIN_NAME_LENGTH 4
#define MAX_NAME_LENGTH 20

bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names return true;
}

bool isNotValid(char ch)
{
	return isalpha(ch) || isdigit(ch) 
		|| ch == '$' || ch == '!' || ch == '.';
}

bool isValidUserName(const std::string& username)
{
	bool valid = false;
	valid = (username.length() >= MIN_NAME_LENGTH && username.length() <= MAX_NAME_LENGTH);
	valid = valid && isalpha(username[0]) && isdigit(username[1]);

	valid = valid && std::find_if(username.begin(), username.end(), isNotValid) != username.end();

	valid = valid && isUniqueName(username);
	
	return valid;
}