#include "Menu.h"

Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(WINDOW_SIZE, WINDOW_SIZE, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	for (int i = 0; i < this->_shapes.size(); i++)
	{
		delete (this->_shapes[i]);
	}
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::options()
{
	int choice = RESTART;

	while (choice != EXIT_CHOICE)
	{
		while (choice > LAST_CHOICE || choice < FIRST_CHOICE)
		{
			system("CLS");
			cout << "Enter 0 to add a new shape." << endl
				<< "Enter 1 to modify or get information from a current shape." << endl
				<< "Enter 2 to delete all of the shapes." << endl
				<< "Enter 3 to exit." << endl;
			cin >> choice;
		}
		this->refresh();

		switch (choice)
		{
		case ADD_SHAPE:
			this->addShape();
			break;
		case MODIFY_SHAPE:
			this->modifyShape();
			break;
		case CLEAR_SHAPES:
			this->clear();
			break;
		case EXIT_CHOICE:
			// Does nothing but exits program later.
			break;
		default:
			break;
		}
		if (choice != EXIT_CHOICE)
		{
			choice = RESTART; // Restart if not exit choice.
		}
	}	
}

void Menu::addShape()
{
	int choice = RESTART;

	while (choice > LAST_CHOICE || choice < FIRST_CHOICE)
	{
		system("CLS");
		cout << "Enter 0 to add a circle." << endl
			<< "Enter 1 to add an arrow." << endl
			<< "Enter 2 to add a triangle."<< endl
			<< "Enter 3 to add a rectangle." << endl;
		cin >> choice;
	}

	switch (choice)
	{
		case ADD_CIRCLE:
			this->addCircl();
			break;
		case ADD_ARROW:
			this->addArrow();
			break;
		case ADD_TRIANGLE:
			this->addTring();
			break;
		case ADD_RECTANGLE:
			this->addRecta();
			break;
		default:
			break;
	}
	this->refresh();
}

void Menu::modifyShape()
{
	unsigned int element = LIMIT_ELE;
	int choice = LIMIT_ELE;

	if (this->_shapes.size())
	{
		while (element >= this->_shapes.size() || element < START_ELE)
		{
			system("CLS");
			for (int i = 0; i < this->_shapes.size(); i++)
			{
				cout << "Enter " << i << " for " << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << endl;
			}
			cin >> element;
		}

		while (choice > MODIFY_LAST || choice < FIRST_CHOICE)
		{
			system("CLS");
			cout << "Enter 0 to move the shape" << endl
				<< "Enter 1 to get its details." << endl
				<< "Enter 2 to remove the shape." << endl;
			cin >> choice;
		}

		switch (choice)
		{
		case MOVE_SHAPE:
			this->moveShape(element);
			break;
		case DETAILS_SHAPE:
			this->shapeDetails(element);
			break;
		case DELETE_SHAPE:
			this->deleteShape(element);
			break;
		default:
			break;
		}
	}
	this->refresh();
}

void Menu::addCircl()
{
	Point center(this->getPoint());
	double radius = 0;
	string name = "";

	cout << "Please enter radius:" << endl;
	cin >> radius;

	name = this->getName();
	
	Circle* newShape = new Circle(center, radius, NAME_CIRC, name);
	this->_shapes.push_back(newShape);
}

void Menu::addArrow()
{
	cout << "Point 1:" << endl;
	Point p1(this->getPoint());
	cout << "Point 2:" << endl;
	Point p2(this->getPoint());

	string name = getName();

	Arrow* newShape = new Arrow(p1, p2, NAME_ARROW, name);
	this->_shapes.push_back(newShape);
}

void Menu::addTring()
{
	cout << "Point 1:" << endl;
	Point p1(this->getPoint());
	cout << "Point 2:" << endl;
	Point p2(this->getPoint());
	cout << "Point 3:" << endl;
	Point p3(this->getPoint());

	string name = getName();

	if (p1.getX() == p2.getX() && p2.getX() == p3.getX() || p1.getY() == p2.getY() && p2.getY() == p3.getY()) // 3 on same line?
	{
		cout << TRIAN_ERROR << endl;
		system("pause");
	}
	else
	{
		Triangle* newShape = new Triangle(p1, p2, p3, NAME_TRI, name);
		this->_shapes.push_back(newShape);
	}
}

void Menu::addRecta()
{
	Point p(this->getPoint());
	double length = 0;
	double width = 0;
	string name = "";

	cout << "Please enter the length of the shape:" << endl;
	cin >> length;

	cout << "Please enter the width of the shape:" << endl;
	cin >> width;

	name = getName();

	myShapes::Rectangle* newShape = new myShapes::Rectangle(p, length, width, NAME_RECT, name);
	this->_shapes.push_back(newShape);
}

Point Menu::getPoint()
{
	double x = 0;
	double y = 0;

	cout << "Please enter X:" << endl;
	cin >> x;
	
	cout << "Please enter Y:" << endl;
	cin >> y;
	
	Point save(x, y);
	return save;
}

string Menu::getName()
{
	string name = "";

	cout << "Please enter the name of the shape: " << endl;
	cin >> name;

	return name;
}

void Menu::shapeDetails(const int element)
{
	this->_shapes[element]->printDetails();

	system("pause");
	this->refresh();
}

void Menu::moveShape(const int element)
{
	system("CLS");

	double x = 0;
	double y = 0;

	cout << "Please enter the X moving scale:";
	cin >> x;
	cout << "Please enter the Y moving scale:";
	cin >> y;

	Point other(x, y);

	this->_shapes[element]->clearDraw(*(this->_disp), *(this->_board));
	this->_shapes[element]->move(other);
}

void Menu::deleteShape(const int element)
{
	this->_shapes[element]->clearDraw(*(this->_disp), *(this->_board));
	delete (this->_shapes[element]);
	this->_shapes.erase(this->_shapes.begin() + element);
	this->refresh();
}

void Menu::refresh()
{
	system("CLS");
	this->clearAll();
	this->drawAll();
}

void Menu::clearAll()
{
	for (unsigned int i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->clearDraw(*(this->_disp), *(this->_board)); // Pointers
	}
}

void Menu::drawAll()
{
	for (unsigned int i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->draw(*(this->_disp), *(this->_board)); // Pointers
	}
}

void Menu::clear()
{
	this->clearAll();
	for (int i = 0; i < this->_shapes.size(); i++)
	{
		delete (this->_shapes[i]);
	}
	this->_shapes.clear();
}