#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

#define NAME_TRI "Triangle"
#define NAME_ARROW "Arrow"
#define NAME_RECT "Rectangle"
#define NAME_CIRC "Circle"
#define RESTART 4
#define LIMIT_ELE 9999
#define CLEAR_SHAPES 2
#define ADD_SHAPE 0 
#define MODIFY_SHAPE 1
#define ADD_CIRCLE 0
#define ADD_ARROW 1
#define ADD_TRIANGLE 2
#define ADD_RECTANGLE 3
#define FIRST_CHOICE 0
#define LAST_CHOICE 3
#define EXIT_CHOICE 3
#define GOOD_EXIT 0 
#define START_ELE 0 
#define MODIFY_LAST 2
#define MOVE_SHAPE 0
#define DETAILS_SHAPE 1
#define DELETE_SHAPE 2
#define WINDOW_SIZE 700
#define TRIAN_ERROR "The points entered create a line."

class Menu
{
public:
	Menu();
	~Menu();
	
	void options(); // displays options.
	void addShape(); // user choice shape to add.
	void modifyShape(); // user choice modify shape.
	void clear(); // Clears elements in shapes.


	void addCircl(); // Adds a Circle to shapes.
	void addArrow(); // Adds a Arrow to shapes.
	void addTring(); // Adds a Tringle to shapes.
	void addRecta(); // Adds a rectangle to shapes.
	 
	void shapeDetails(const int element); // Shapes details.
	void moveShape(const int element); // Moves place of shape.
	void deleteShape(const int element); // Deletes a shape.

	void clearAll(); // Clears all shapes from board
	void drawAll(); // Draws all shapes to board.
	void refresh(); // Refreshes screen.

private: 
	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;

	static Point getPoint();
	static string getName();
};

