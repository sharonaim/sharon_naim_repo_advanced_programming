#include "Polygon.h"

Polygon::Polygon(const string& type, const string& name) : Shape(name, type)
{
}

Polygon::~Polygon()
{
}

void Polygon::move(const Point& other)
{
	int size = this->_points.size();

	for (int i = 0; i < size; i++)
	{
		this->_points[i] += other;
	}
}


double Polygon::getPerimeter() const
{
	int size = this->_points.size();
	double p = 0;
	
	for (int i = 0; i < size; i++)
	{
		p += this->_points[i].distance(this->_points[i % size]); // so -> the last point will call the first.
	}
	
	return p;
}