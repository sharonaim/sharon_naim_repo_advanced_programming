#include "Rectangle.h"

namespace myShapes
{
	Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(type, name)
	{
		Point b(length, width);
		b += a;
		this->_points.push_back(a);
		this->_points.push_back(b);
	}

	Rectangle::~Rectangle()
	{
	}

	double Rectangle::getArea() const
	{
		double width = this->_points[0].getX() - this->_points[1].getX();
		double length = this->_points[0].getY() - this->_points[1].getY();
		return width * length;
	}

	void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
	{
		unsigned char WHITE[] = { 255, 255, 255 };
		board.draw_rectangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
	}

	void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
	{
		unsigned char BLACK[] = { 0, 0, 0 };
		board.draw_rectangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
	}
}