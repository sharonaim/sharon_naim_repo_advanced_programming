#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

double Triangle::getArea() const
{
	double a = 0;
	double b = 0;
	double c = 0;
	double sum = 0;
	
	a = this->_points[0].distance(this->_points[2]);
	b = this->_points[0].distance(this->_points[1]);
	c = this->_points[1].distance(this->_points[2]);

	sum = (a + b + c) / 2;
	return sqrt(sum * (sum - a) * (sum - b) * (sum - c));
}