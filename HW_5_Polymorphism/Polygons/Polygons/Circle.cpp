#include "Circle.h"

Circle::Circle(const Point& center, double radius, const string& type, const string& name) : Shape(name, type), _center(center)
{
	this->_radius = radius;
}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return pow(this->_radius, 2) * PI;
}

double Circle::getPerimeter() const
{
	return this->_radius * PI * 2;
}

void Circle::move(const Point& other) // add the Point to all the points of shape
{
	this->_center += other;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}