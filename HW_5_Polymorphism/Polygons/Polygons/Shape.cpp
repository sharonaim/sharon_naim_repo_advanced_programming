#include "Shape.h"

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

void Shape::printDetails() const
{
	std::cout << this->_type.c_str() 
		<< "\t" << this->_name.c_str() 
		<< "\t" << this->getArea() 
		<< "\t" << this->getPerimeter() << std::endl;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}