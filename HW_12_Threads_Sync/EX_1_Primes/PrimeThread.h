#pragma once
#include <thread>
#include <iostream>
#include <vector>
#include <windows.h>

bool isPrime(int num);

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int currNum = begin < 2 ? 2 : begin;

	for (int i = currNum; i <= end; i++)
	{
		if (isPrime(i))
		{
			primes.push_back(i);
		}
	}
}

bool isPrime(int num)
{
	int max = sqrt(num);

	if (!(num % 2) && num != 2)
	{
		return false;
	}
	
	for(int count = 3; count <= max; count += 2)
	{
		if (!(num % count))
		{
			return false;
		}
	}
	return true;
}

void printVector(std::vector<int>& primes)
{
	for (std::vector<int>::iterator it = primes.begin(); it != primes.end(); ++it)
	{
		std::cout << *it << ", ";
	}
	std::cout << std::endl;
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;

	std::thread calcPrime(getPrimes, begin, end, std::ref(primes));
	calcPrime.join();

	return primes;
}