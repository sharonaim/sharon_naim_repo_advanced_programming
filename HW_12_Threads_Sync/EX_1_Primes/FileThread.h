#pragma once
#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include "PrimeThread.h"

using std::ofstream;
using std::string;

std::mutex mtx;

void writePrimesToFile(int begin, int end, ofstream& file)
{
	std::vector<int> primes = callGetPrimes(begin, end);
	std::vector<int>::iterator endo = primes.end();

	for (std::vector<int>::iterator it = primes.begin(); it != endo; ++it)
	{
		mtx.lock();
		file << *it << " ,";
		mtx.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream myfile;
	myfile.open(filePath);

	LARGE_INTEGER timeStart;
	LARGE_INTEGER timeEnd;
	LARGE_INTEGER frequency;
	double interval;

	std::thread* myThreads = new std::thread[N];

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&timeStart);

	if (myfile.is_open())
	{
		for (int i = 0; i < N; i++)
		{
			myThreads[i] = std::thread(writePrimesToFile, begin + i * end / N, (i + 1) * end / N, std::ref(myfile));
		}

		for (int i = 0; i < N; i++)
		{
			myThreads[i].join();
		}
		myfile.close();
	}

	QueryPerformanceCounter(&timeEnd);
	interval = (double)(timeEnd.QuadPart - timeStart.QuadPart) / frequency.QuadPart;

	
	std::cout << "The time it took to calculate all primes from" << std::endl
		<< begin << " to " << end << " and write them to file was - " << interval << std::endl;
	delete[] myThreads;
}