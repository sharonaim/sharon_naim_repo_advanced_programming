#include "Menu.h"


Menu::Menu()
{
	this->_start = false;
}

Menu::~Menu()
{
}

void Menu::setup()
{
	// creating the thread and letting them go, then starting the user interface.
	std::thread logDequeue = std::thread(LogDequeuer::dequeueMassage, OUTPUT, std::ref(this->_messages), std::ref(this->_users));
	std::thread dataQueue = std::thread(DataQueuer::dataRead, DATA_READ, std::ref(this->_messages), std::ref(this->_starter));

	logDequeue.detach();
	dataQueue.detach();
	this->openingScreen();
}

void Menu::openingScreen()
{
	try
	{
		int choice = 0;
	
		do {
			std::cout << "Welcome to MagshiSender!" << std::endl
				<< "1. to sign in a user" << std::endl
				<< "2. to sign out a user" << std::endl
				<< "3. to print connected user" << std::endl
				<< "4. to exit" << std::endl;
			std::cin >> choice;
		} while (choice > EXIT || choice < SIGN_IN);
		this->clearScreen();

		switch (choice)
		{
		case SIGN_IN:
			this->newUser();
			break;
		case SIGN_OUT:
			this->deleteUser();
			break;
		case CONNECTED_USERS:
			this->printUsers();
			break;
		case EXIT:
			this->exitMenu();
			break;
		default:
			_exit(ERROR_EXIT);
			break;
		}
	}
	catch (std::exception& except)
	{
		std::cout << except.what() << std::endl;
	}
	catch (...)
	{
		std::cout << UNKNOWN_ERR << std::endl;
	}
	this->retOpening();
}

void Menu::newUser()
{
	string name;
	
	std::cout << "Enter username: ";
	std::cin >> name;
	
	if (this->inSet(name))
	{
		throw NameException();
	}
	else
	{
		mtx.lock();
		this->_users.insert(name);
		mtx.unlock();
		if (!this->_start)
		{ // if it is the first ever user, 
		//then telling the data queuer to start going, so it wouldn't queue for nothing.
			this->_starter.notify_all();
			this->_start = true;
		}
		std::cout << "User inserted..." << std::endl;
	}
}

void Menu::deleteUser()
{
	string name;

	std::cout << "Enter the customer's name: ";
	std::cin >> name;

	if (!this->inSet(name))
	{
		throw AccessException();
	}
	else
	{
		mtx.lock();
		this->_users.erase(name);
		mtx.unlock();
		std::cout << "User deleted..." << std::endl;
	}
}

void Menu::printUsers()
{
	for (set<string>::iterator it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		std::cout << "Name - " << *it << std::endl;
	}
}

bool Menu::inSet(const string name)
{
	return this->_users.find(name.c_str()) != this->_users.end();
}

void Menu::exitMenu()
{
	_exit(GOOD_EXIT);
}

void Menu::clearScreen()
{
	system("cls");
}

void Menu::retOpening()
{
	system("pause");
	system("cls");
	this->openingScreen();
}