#include <fstream>
#include <thread>
#include <chrono>
#include <string>
#include <queue>
#include <iostream>
#include <mutex>
#include <condition_variable>
#pragma once

#define MINUTE 15

using std::string;
using std::queue;

// creating mutex lock.
extern std::mutex mtx;
extern std::condition_variable condition;

class DataQueuer
{
public:
	static void dataRead(const string path, queue<string>& myQueue, std::condition_variable& starter);
	static void queuer(const string toQueue, queue<string>& myQueue);
	static void clearFile(const string path);
	static void printQueue(queue<string>& myQueue);
};