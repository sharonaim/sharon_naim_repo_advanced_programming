#include "DataQueuer.h"

void DataQueuer::dataRead(const string path, queue<string>& myQueue, std::condition_variable& starter)
{
	std::chrono::seconds interval(MINUTE);
	std::mutex oneTimeLock;
	std::unique_lock<std::mutex> startLock(oneTimeLock);

	// waiting till there's the first user.
	starter.wait(startLock);
	startLock.release();
	while (true)
	{
		std::ifstream file(path);
		if (file.is_open())
		{
			std::string line;
			while (getline(file, line))
			{ // reading each line and then sending it to queuer.
				mtx.lock();
				queuer(line, myQueue);
				mtx.unlock();
				condition.notify_all(); // now there's messages in queue and can write to log.
			}
			file.close();
			clearFile(path);
		}
		std::this_thread::sleep_for(interval);
	}
}

void DataQueuer::queuer(const string toQueue, queue<string>& myQueue)
{ // queuing.
	myQueue.push(toQueue);
}

void DataQueuer::clearFile(const string path)
{ // running over all the data in file.
	std::ofstream ofs;
	ofs.open(path, std::ofstream::out | std::ofstream::trunc);
	ofs.close();
}

void DataQueuer::printQueue(queue<string>& myQueue)
{
	queue<string> save = myQueue;

	while (!save.empty())
	{
		std::cout << save.front() << std::endl;
		save.pop();
	}
}