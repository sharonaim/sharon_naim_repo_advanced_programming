#pragma once
#include "DataQueuer.h"
#include <set>

using std::set;

class LogDequeuer
{
public:
	static void dequeueMassage(const string path, queue<string>& myQueue, set<string>& users);
};