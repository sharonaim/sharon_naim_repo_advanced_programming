#include "LogDequeuer.h"

void LogDequeuer::dequeueMassage(const string path, queue<string>& myQueue, set<string>& users)
{
	std::mutex waitDataLoad;
	std::unique_lock<std::mutex> myLock(waitDataLoad);
	std::ofstream file;
	file.open(path);

	if (file.is_open())
	{
		file.close();
		while (true)
		{
			// waiting until the data was queued.
			condition.wait(myLock);
			file.open(path, std::ofstream::app);
			
			while (!myQueue.empty())
			{ // getting all massages out.
				mtx.lock();
				string msg = myQueue.front();
				myQueue.pop();
				mtx.unlock();

				for (set<string>::iterator it = users.begin(); it != users.end(); ++it)
				{
					mtx.lock();
					file << *it << ": " << msg << std::endl;
					mtx.unlock();
				}
			}
			file.close();
		}
	}
}