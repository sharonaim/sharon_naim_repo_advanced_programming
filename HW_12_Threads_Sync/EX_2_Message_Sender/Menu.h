#include <set>
#include <iostream>
#include <string>
#include "UserExceptions.h"
#include "DataQueuer.h"
#include "LogDequeuer.h"
#pragma once

#define OUTPUT "output.txt"
#define DATA_READ "data.txt"

using std::set;
using std::string;

enum openings{SIGN_IN = 1, SIGN_OUT, CONNECTED_USERS, EXIT};
enum exits{GOOD_EXIT, ERROR_EXIT};

class Menu
{
public:
	Menu();
	~Menu();

	void setup(); // threads and all.
	void openingScreen();
	void newUser();
	void deleteUser();
	void printUsers();
	void exitMenu();

private:
	set<string> _users;
	queue<string> _messages;
	bool _start;
	std::condition_variable _starter;
	
	static void clearScreen();

	void retOpening();
	bool inSet(const string name);
};