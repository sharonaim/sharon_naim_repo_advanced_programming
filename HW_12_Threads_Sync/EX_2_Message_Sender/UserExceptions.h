#pragma once
#include <exception>

#define ERR_NAME "That user is already connected to our server..."
#define ERR_ACCESS "Such user does not exist..."
#define UNKNOWN_ERR "An unkown error has occurred..."

class NameException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_NAME;
	}
};

class AccessException : public std::exception
{
	virtual const char* what() const
	{
		return ERR_ACCESS;
	}
};