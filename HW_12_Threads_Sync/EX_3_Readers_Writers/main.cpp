#include "readersWriters.h"

enum choices {READ=1, WRITE, EXIT};

void printMenu();
void read(readersWriters& manager);
void write(readersWriters& manager);

int main()
{
	int choice = 0;
	readersWriters manager("data.txt");

	while (choice != EXIT)
	{
		printMenu();
		std::cin >> choice;

		switch (choice)
		{
		case READ:
			read(manager);
			break;
		case WRITE:
			write(manager);
			break;
		default:
			break;
		}
	}

	system("pause");
	return 0;
}

void printMenu()
{
	system("CLS");
	std::cout << "1 - Read a line from file." << std::endl
		<< "2 - Write a line to file." << std::endl
		<< "3 - Exit." << std::endl;
}

void read(readersWriters& manager)
{
	int line;

	system("CLS");
	std::cout << "Please enter line to read - " << std::endl;
	std::cin >> line;
	manager.readLine(line);
	system("pause");
}

void write(readersWriters& manager)
{
	int line;
	std::string str;

	system("CLS");
	std::cout << "Please enter line to write - " << std::endl;
	std::cin >> line;
	std::cout << "Please enter what to write - " << std::endl;
	std::cin >> str;
	manager.WriteLine(line - 1, str);

	std::cout << "Line Written" << std::endl;
	system("pause");
}