#include "readersWriters.h"

readersWriters::readersWriters(std::string fileName)
{
	this->_fileName = fileName;
	this->_readersNumber = 0;
	this->_writersNumber = 0;
	this->_locker = std::unique_lock<std::mutex>(this->_mu);
	this->_printThread = std::thread (&readersWriters::printer, this);
	this->_printThread.detach();
}

readersWriters::~readersWriters()
{ // releasing the waiting of the printer
	this->_locker.release();
}

void readersWriters::printer()
{
	while (true)
	{
		this->_condP.wait(this->_locker);

		while (!this->_prints.empty())
		{
			std::cout << this->_prints.front() << std::endl;
			this->_prints.pop();
		}
	}
}

void readersWriters::readLock()
{
	if (this->_writersNumber) // if there's a writer
	{
		std::mutex oneTimeLock;
		std::unique_lock<std::mutex> startLock(oneTimeLock);
		this->_condR.wait(startLock);
	}
	this->_readersNumber++;
}

void readersWriters::writeLock()
{
	while (this->_writersNumber || this->_readersNumber) // if there's a writer
	{
		std::mutex oneTimeLock;
		std::unique_lock<std::mutex> startLock(oneTimeLock);
		this->_condW.wait(startLock);
	}
	this->_writersNumber++;
}

void readersWriters::readUnlock()
{
	this->_readersNumber--;
	if (!this->_readersNumber)
	{
		this->_condW.notify_one();
	}
}

void readersWriters::writeUnlock()
{
	this->_writersNumber--;
	this->_condR.notify_all(); // must only be one everytime, so noone is writing for sure.
	this->_condW.notify_one(); // when there are only writers.
}

void readersWriters::readLine(int lineNumber)
{
	std::thread myThread(&readersWriters::threadRead, this, lineNumber);
	myThread.detach();
}

void readersWriters::WriteLine(int lineNumber, std::string newLine)
{
	std::thread myThread(&readersWriters::threadWrite, this, lineNumber, newLine);
	myThread.detach();
}

void readersWriters::queueLine(std::string line)
{
	this->_mu.lock(); // adding to prints vecter.
	this->_prints.push(line);
	this->_mu.unlock();
	this->_condP.notify_all(); // telling to print
}

void readersWriters::threadRead(int lineNumber)
{
	this->readLock();

	int number_of_line = 1;
	std::string line;
	std::ifstream myfile(this->_fileName);

	while (std::getline(myfile, line))
	{
		if (number_of_line == lineNumber)
		{
			queueLine(line);
			break;
		}
		else
		{
			number_of_line++;
		}
	}
	if (number_of_line != lineNumber)
	{
		this->queueLine("the requested line doesn't exist.");
	}

	this->readUnlock();
}

void readersWriters::threadWrite(int lineNumber, std::string newLine)
{
	this->writeLock();

	std::fstream file(this->_fileName);
	if (file)
	{
		unsigned currentLine = 0;
		while (currentLine < lineNumber)
		{
			// Till we reach our destination.
			file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			++currentLine;
		}

		file.seekp(file.tellg());
		file << newLine;
		file.close();
	}

	this->writeUnlock();
}

int readersWriters::lines()
{ // we know the file is locked for the writer.
	int number_of_lines = 0;
	std::string line;
	std::ifstream myfile(this->_fileName);

	while (std::getline(myfile, line))
	{
		number_of_lines++;
	}

	myfile.close();
	return number_of_lines;
}