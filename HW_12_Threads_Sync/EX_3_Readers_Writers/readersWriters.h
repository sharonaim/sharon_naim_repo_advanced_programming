#pragma once
#include <fstream>
#include <mutex>
#include <condition_variable>
#include <string>
#include <queue>
#include <iostream>
#include <limits>


class readersWriters
{
public:
	readersWriters(std::string fileName);
	~readersWriters();
	void readLock();
	void writeLock();
	void readUnlock();
	void writeUnlock();
	void readLine(int lineNumber); //lineNumber - line number to read
	void WriteLine(int lineNumber, std::string newLine);//lineNumber - line number to write 
	void threadRead(int lineNumber);
	void threadWrite(int lineNumber, std::string newLine);
	int lines();
	void queueLine(std::string line);
	void printer();

private:
	std::queue<std::string> _prints;
	std::mutex _mu;
	std::unique_lock<std::mutex> _locker;
	std::condition_variable _condW;
	std::condition_variable _condR;
	std::condition_variable _condP;
	std::thread _printThread;
	int _readersNumber;
	int _writersNumber;
	std::string _fileName;
};