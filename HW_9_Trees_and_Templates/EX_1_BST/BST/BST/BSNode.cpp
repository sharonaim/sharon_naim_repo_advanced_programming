#include "BSNode.h"

BSNode::BSNode(string data)
{
	this->_data = data;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	*(this) = other;
}

BSNode::~BSNode()
{
	if (this)
	{
		delete this->_left;
		delete this->_right;
	}
}

void BSNode::insert(string value)
{
	if (this->_data == value)
	{
		this->_count++;
	}
	else
	{
		this->actualInsert(value);
	}
}

void BSNode::actualInsert(string value)
{
	if (this->_data < value)
	{
		if (this->_right)
		{
			if (this->_right->_data == value)
			{
				this->_right->_count += ANOTHER_ONE;
			}
			else
			{
				this->_right->insert(value);
			}
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
	else
	{
		if (this->_left)
		{
			if (this->_left->_data == value)
			{
				this->_left->_count += ANOTHER_ONE;
			}
			else
			{
				this->_left->insert(value);
			}
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	this->copy(&other);
	return *(this);
}

void BSNode::copy(const BSNode* other)
{
	if (other)
	{
		this->insert(other->_data);
		this->copy(this->_right);
		this->copy(this->_left);
	}
}

bool BSNode::isLeaf() const
{
	return !this->_left && !this->_right;
}

string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{
	if (!this)
	{
		return false;
	}
	if (this->_data == val)
	{
		return true;
	}
	else if (this->isLeaf())
	{
		return false;
	}
	else
	{
		return this->_left->search(val) || this->_right->search(val);
	}
}

int BSNode::getHeight() const
{
	if (!this)
	{
		return END_TREE;
	}
	if (this->isLeaf())
	{
		return NEXT_BRANCH;
	}
	else
	{
		int lenr = NEXT_BRANCH + this->_right->getHeight();
		int lenl = NEXT_BRANCH + this->_left->getHeight();

		return lenr > lenl ? lenr : lenl;
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this, 0);
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node, int count) const
{
	if (!this)
	{
		return NOT_FOUND;
	}
	else if (this == node)
	{
		return count + NEXT_BRANCH;
	}
	else
	{
		int depr = this->_right->getCurrNodeDistFromInputNode(node, count + NEXT_BRANCH);
		int depl = this->_left->getCurrNodeDistFromInputNode(node, count + NEXT_BRANCH);


		return depr > depl ? depr : depl;
	}
}

void BSNode::printNodes() const
{
	if (this)
	{
		this->_left->printNodes();

		std::cout << this->_data << ", Count: " << this->_count << std::endl;

		this->_right->printNodes();
	}
}