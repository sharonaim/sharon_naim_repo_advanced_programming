#include <string>
#include <iostream>
#pragma once

#define NOT_FOUND 0
#define END_TREE 0
#define NEXT_BRANCH 1
#define ANOTHER_ONE 1

using std::string;

class BSNode
{
public:
	BSNode(string data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B

	void copy(const BSNode* other);
	void actualInsert(string value);
	int getCurrNodeDistFromInputNode(const BSNode* node, int count) const; //auxiliary function for getDepth
};