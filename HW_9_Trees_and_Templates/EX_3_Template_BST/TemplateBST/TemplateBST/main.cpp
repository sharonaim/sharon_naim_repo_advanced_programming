#include "BSNode.h"
#include <iostream>
#include <string>

#define ARR_SIZE 10

using std::cout;
using std::endl;
using std::string;

int main()
{
	string arrString[ARR_SIZE] = { "Z", "F", "E", "W", "B", "N", "J", "O", "L", "P"};
	int arrInt[ARR_SIZE] = { 12, 3, 42, 52, 7, 15, 163, 132, 95, 23};

	BSNode<string>* bs = new BSNode<string>(arrString[0]);
	BSNode<int>* bs2 = new BSNode<int>(arrInt[0]);

	for (int i = 1; i < ARR_SIZE; i++)
	{
		bs->insert(arrString[i]);
	}

	
	cout << "Tree height: " << bs->getHeight() << endl;

	bs->printNodes();

	for (int i = 1; i < ARR_SIZE; i++)
	{
		bs2->insert(arrInt[i]);
	}


	cout << endl << "Tree height: " << bs2->getHeight() << endl;

	bs2->printNodes();

	system("pause");
	delete bs;
	delete bs2;

	return 0;
}