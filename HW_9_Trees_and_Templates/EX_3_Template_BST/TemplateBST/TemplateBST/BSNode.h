#include <iostream>
#pragma once

#define NOT_FOUND 0
#define END_TREE 0
#define NEXT_BRANCH 1
#define ANOTHER_ONE 1

template<class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B

	void copy(const BSNode* other);
	void actualInsert(T value);
	int getCurrNodeDistFromInputNode(const BSNode* node, int count) const; //auxiliary function for getDepth
};

template<class T>
BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_count = 1;
}

template<class T>
BSNode<T>::BSNode(const BSNode& other)
{
	*(this) = other;
}

template<class T>
BSNode<T>::~BSNode()
{
	if (this)
	{
		delete this->_left;
		delete this->_right;
	}
}

template<class T>
void BSNode<T>::insert(T value)
{
	if (this->_data == value)
	{
		this->_count++;
	}
	else
	{
		this->actualInsert(value);
	}
}

template<class T>
void BSNode<T>::actualInsert(T value)
{
	if (this->_data < value)
	{
		if (this->_right)
		{
			if (this->_right->_data == value)
			{
				this->_right->_count += ANOTHER_ONE;
			}
			else
			{
				this->_right->insert(value);
			}
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
	else
	{
		if (this->_left)
		{
			if (this->_left->_data == value)
			{
				this->_left->_count += ANOTHER_ONE;
			}
			else
			{
				this->_left->insert(value);
			}
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
}

template<class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	this->copy(&other);
	return *(this);
}

template<class T>
void BSNode<T>::copy(const BSNode* other)
{
	if (other)
	{
		this->insert(other->_data);
		this->copy(this->_right);
		this->copy(this->_left);
	}
}

template<class T>
bool BSNode<T>::isLeaf() const
{
	return !this->_left && !this->_right;
}

template<class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template<class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template<class T>
bool BSNode<T>::search(T val) const
{
	if (!this)
	{
		return false;
	}
	if (this->_data == val)
	{
		return true;
	}
	else if (this->isLeaf())
	{
		return false;
	}
	else
	{
		return this->_left->search(val) || this->_right->search(val);
	}
}

template<class T>
int BSNode<T>::getHeight() const
{
	if (!this)
	{
		return END_TREE;
	}
	if (this->isLeaf())
	{
		return NEXT_BRANCH;
	}
	else
	{
		int lenr = NEXT_BRANCH + this->_right->getHeight();
		int lenl = NEXT_BRANCH + this->_left->getHeight();

		return lenr > lenl ? lenr : lenl;
	}
}

template<class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this, 0);
}

template<class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode* node, int count) const
{
	if (!this)
	{
		return NOT_FOUND;
	}
	else if (this == node)
	{
		return count + NEXT_BRANCH;
	}
	else
	{
		int depr = this->_right->getCurrNodeDistFromInputNode(node, count + NEXT_BRANCH);
		int depl = this->_left->getCurrNodeDistFromInputNode(node, count + NEXT_BRANCH);


		return depr > depl ? depr : depl;
	}
}

template<class T>
void BSNode<T>::printNodes() const
{
	if (this)
	{
		this->_left->printNodes();

		std::cout << this->_data << " ";

		this->_right->printNodes();
	}
}