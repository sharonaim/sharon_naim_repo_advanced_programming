#pragma once

#define FIRST -1
#define SECOND 1
#define EQUAL 0

template<class T>
int compare(T comp1, T comp2)
{
	if (comp1 > comp2)
	{
		return FIRST;
	}
	else if (comp2 == comp1)
	{
		return EQUAL;
	}
	else
	{
		return SECOND;
	}
}

template<class T>
void swap(T& obj1, T& obj2)
{
	T save = obj1;
	obj1 = obj2;
	obj2 = save;
}

template<class T>
void bubbleSort(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (arr[i] < arr[j])
			{
				swap(arr[i], arr[j]);
			}
		}
	}
}

template<class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}