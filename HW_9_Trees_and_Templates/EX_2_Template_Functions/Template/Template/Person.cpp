#include "Person.h"

Person::Person(int age, string name)
{
	this->_name = name;
	this->_age = age;
}

int Person::getAge() const
{
	return this->_age;
}

string Person::getName() const
{
	return this->_name;
}

bool Person::operator<(Person& other)
{
	return this->_age < other._age;
}

bool Person::operator>(Person& other)
{
	return this->_age > other._age;
}

bool Person::operator==(Person& other)
{
	return this->_age == other._age;
}

std::ostream& operator<<(std::ostream& stream, const Person& person)
{
	stream << "Name: " << person.getName() << ", Age: " << person.getAge();
	return stream;
}