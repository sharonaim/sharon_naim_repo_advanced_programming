#include "functions.h"
#include "Person.h"
#include <iostream>

int main() 
{
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;


//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	char charArr[arr_size] = { 'D', 'E', 'A', 'C', 'B' };
	bubbleSort<char>(charArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

//my object
	const int size = 3;
	Person personArr[size] = {Person(18, "Ellenys"), Person(30, "Sikelle"), Person(9, "Pews")};

	printArray<Person>(personArr, size);

	bubbleSort<Person>(personArr, size);
	std::cout << std::endl << "Sorting..." << std::endl << std::endl;

	printArray<Person>(personArr, size);

	system("pause");
	return 0;
}