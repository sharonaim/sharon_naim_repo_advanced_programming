#pragma once
#include <string>
#include <iostream>

using std::string;

class Person
{
public:
	Person(int age, string name);

	int getAge() const;
	string getName() const;

	bool operator<(Person& other);
	bool operator>(Person& other);
	bool operator==(Person& other);

	friend std::ostream& operator<<(std::ostream& stream, const Person& person);

private:
	string _name;
	int _age;
};

