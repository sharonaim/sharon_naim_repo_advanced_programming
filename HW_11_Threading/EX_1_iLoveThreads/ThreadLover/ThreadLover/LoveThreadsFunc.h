#pragma once
#include <iostream>
#include <thread>

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread loveThread(I_Love_Threads);
	loveThread.join();
}