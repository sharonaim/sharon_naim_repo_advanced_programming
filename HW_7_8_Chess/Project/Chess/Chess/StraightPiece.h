#pragma once
#include "ChessPiece.h"

class StraightPiece : public ChessPiece
{
public:
	StraightPiece(Point& place, char type, bool side);

protected:
	virtual bool checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	virtual bool checkExtraMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]); // only when need will be used.
};