#pragma once
#include "ChessPiece.h"

#define RANGE_KNIGHT sqrt(5)

class Knight : public ChessPiece
{
public:
	Knight(Point& place, bool side);
	virtual bool checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
};