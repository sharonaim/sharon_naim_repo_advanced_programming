#pragma once
#include "Point.h"
#include <math.h>

#define BOARD_SIZE 8

class ChessPiece
{
public:
	ChessPiece(Point& place, char type, bool side);

	void changePlace(int y, int x);

	Point getPlace() const;
	char getType() const;
	bool getSide() const;

	bool through(ChessPiece* toGo);
	bool isFresh() const;

	virtual bool justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	virtual bool boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]);

protected:
	Point _place;
	char _type;
	bool _side;
	bool _fresh;

	double distance(const Point& src, const Point& dst);
	virtual bool checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE]) = 0;
	virtual bool checkExtraMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]); // only when need will be used.
};