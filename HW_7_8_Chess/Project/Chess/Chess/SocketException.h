#pragma once
#include <exception>

class SocketException : public std::exception
{
	virtual const char* what() const
	{
		return "Exception: Inavlid text from socket.";
	}
};
