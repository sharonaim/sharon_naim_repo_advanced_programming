#include "Game.h"
#include "Point.h"
#include <stdio.h>
#include <stdlib.h>

#define FIRST_INDEX 0
#define SECOND_INDEX 1
#define DECIMAL 10
#define ANS_TO_SOCK_LEN 2
#define FIVE_SEC_SLEEP 5000

Game::Game()
{
	this->_isRunning = true;
	this->_turn = true;
}

void Game::gameRun()
{
	srand(time_t(NULL));
	Pipe p;
	string ans;
	bool isConnect = p.connect();
	while (!isConnect)
	{
		ans = printReconnect();
		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(FIVE_SEC_SLEEP);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}
	// msgToGraphics should contain the board string accord the protocol
	try
	{
		strcpy_s(this->_msgToGraphics, START_PIECE);
		p.sendMessageToGraphics(this->_msgToGraphics);   // send the board string
		// get message from graphics
		this->_msgFromGraphics = p.getMessageFromGraphics();
		checkMsgFromSocket();
		while (this->_msgFromGraphics != QUIT_MSG)
		{
			sendToPiece();
			//strcpy_s(this->_msgToGraphics, "0");
			p.sendMessageToGraphics(this->_msgToGraphics);
			// get message from graphics
			this->_msgFromGraphics = p.getMessageFromGraphics();
			checkMsgFromSocket();
		}
	}
	catch (exception & e)
	{
		cout << e.what() << endl;
	}
	p.close();
}

string Game::printReconnect()
{
	string ans;
	cout << "cant connect to graphics" << endl;
	cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
	cin >> ans;
	while (ans != "0" && ans != "1")
	{
		cout << "Inavlid Input." << endl;
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;
	}
	return ans;
}

void Game::checkMsgFromSocket()
{
	if (this->_msgFromGraphics.length() < SOCKET_MSG_LEN)
	{
		throw SocketException();
	}
}

void Game::sendToPiece()
{
	
	char charCase[ANS_TO_SOCK_LEN] = {0};
	memset(this->_msgToGraphics,0,MSG_SIZE);
	Point P_src = changeToPoint(this->_msgFromGraphics[FIRST_INDEX], this->_msgFromGraphics[SECOND_INDEX]);
	Point P_dst = changeToPoint(this->_msgFromGraphics[2], this->_msgFromGraphics[3]);
	try
	{
		charCase[FIRST_INDEX] = this->_arena.tryMove(P_src, P_dst, this->_turn);
		this->_msgToGraphics[FIRST_INDEX] = charCase[FIRST_INDEX];
		this->_msgToGraphics[SECOND_INDEX] = NULL;
		cout << this->_msgToGraphics << endl;
		this->_turn = !(this->_turn);
	}
	catch (moves n)
	{
		_itoa(n,charCase,DECIMAL);
		this->_msgToGraphics[FIRST_INDEX] = charCase[FIRST_INDEX];
		this->_msgToGraphics[SECOND_INDEX] = NULL;
	}
}

Point Game::changeToPoint(char ch, char num)
{
	int num1 = int(ch - 'a');
	int num2 = int(num - '0' - 1);
	return Point(num1, BOARD_SIZE - 1 - num2);
}