#include "ChessPiece.h"

ChessPiece::ChessPiece(Point& place, char type, bool side) : _place(place)
{
	this->_type = type;
	this->_side = side;
	this->_fresh = true;
}

void ChessPiece::changePlace(int y, int x)
{
	this->_place.setX(x);
	this->_place.setY(y);
}

Point ChessPiece::getPlace() const
{
	return this->_place;
}

char ChessPiece::getType() const
{
	return this->_type;
}

bool ChessPiece::isFresh() const
{
	return this->_fresh;
}

bool ChessPiece::getSide() const
{
	return this->_side;
}

bool ChessPiece::checkExtraMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return true;
}

bool ChessPiece::through(ChessPiece* toGo)
{
	return toGo == nullptr; // To check if can pass through.
}

double ChessPiece::distance(const Point& src, const Point& dst)
{
	return sqrt(pow(dst.getX() - src.getX(), 2) + pow(dst.getY() - src.getY(), 2));
}

bool ChessPiece::justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return this->checkMovement(dst, board);
}

bool ChessPiece::boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	if (this->checkMovement(dst, board))
	{
		this->_place = dst;
		return true;
	}
	return false;
}