#include "Point.h"

Point::Point(int x, int y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	*this = other;
}

void Point::setX(const int x)
{
	this->_x = x;
}
void Point::setY(const int y)
{
	this->_y = y;
}

int Point::getX() const
{
	return this->_x;
}
int Point::getY() const
{
	return this->_y;
}

void Point::add(int x, int y)
{
	this->_x += x;
	this->_y += y;
}

Point& Point::operator+=(const Point& other)
{
	this->add(other._x, other._y);
	return *this;
}

Point& Point::operator=(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
	return *this;
}

bool Point::operator==(const Point& other) const
{
	return this->_x == other._x && this->_y == other._y;
}