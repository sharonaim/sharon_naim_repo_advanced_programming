#pragma once
#include "StraightPiece.h"

class Rook : public StraightPiece
{
public:
	Rook(Point& place, bool side);
	virtual bool justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	virtual bool boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
};