#pragma once
class Point
{
public:
	Point(int x, int y);
	Point(const Point& other);

	void setX(const int x);
	void setY(const int y);

	int getX() const;
	int getY() const;

	void add(int x, int y);

	Point& operator+=(const Point& other);
	Point& operator=(const Point& other);
	bool operator==(const Point& other) const;

private:
	int _x;
	int _y;
};