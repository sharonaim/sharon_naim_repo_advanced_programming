#include "Rook.h"

Rook::Rook(Point& place, bool side) : StraightPiece(place, side ? 'R' : 'r', side)
{
	this->_fresh = true;
}

bool Rook::justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return StraightPiece::checkMovement(dst, board);
}

bool Rook::boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	if (StraightPiece::checkMovement(dst, board))
	{
		this->_place = dst;
		this->_fresh = false;
		return true;
	}
	return false;
}