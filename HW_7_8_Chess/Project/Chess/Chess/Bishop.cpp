#include "Bishop.h"

Bishop::Bishop(Point& place, bool side) : StraightPiece(place, side ? 'B' : 'b', side)
{
}

bool Bishop::justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return StraightPiece::checkExtraMove(dst, board);
}

bool Bishop::boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	if (StraightPiece::checkExtraMove(dst, board))
	{
		this->_place = dst;
		return true;
	}
	return false;
}