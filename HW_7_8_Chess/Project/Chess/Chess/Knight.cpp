#include "Knight.h"

Knight::Knight(Point& place, bool side) : ChessPiece(place, side ? 'N' : 'n', side)
{
}

bool Knight::checkMovement(const Point& dst, ChessPiece* board[][BOARD_SIZE])
{
	return distance(this->_place, dst) == RANGE_KNIGHT;
}