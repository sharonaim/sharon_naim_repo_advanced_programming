#pragma once
#include "StraightPiece.h"

class Bishop : public StraightPiece
{
public:
	Bishop(Point& place, bool side);
	virtual bool justChecking(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
	virtual bool boardCheckMove(const Point& dst, ChessPiece* board[][BOARD_SIZE]);
};