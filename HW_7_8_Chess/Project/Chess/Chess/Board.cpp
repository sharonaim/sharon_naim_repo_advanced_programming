#include "Board.h"
#include <iostream>

Board::Board()
{
	this->_turn = true;
	this->startingSetup(); 
}

void Board::startingSetup()
{
	this->resetBoard();
	this->setPawn();
	this->setUpper();
}

void Board::resetBoard()
{
	for (int i = 0; i < BOARD_SIZE; i++) // resetting board first.
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			this->_board[i][j] = nullptr;
		}
	}
}

void Board::setPawn()
{
	Point hold(0, LINE_PAWN_BLACK);
	for (int x = 0; x < BOARD_SIZE; x++)
	{
		hold.setX(x);
		this->_board[LINE_PAWN_BLACK][x] = new Pawn(hold, false);
	}

	hold.setY(LINE_PAWN_WHITE);

	for (int x = 0; x < BOARD_SIZE; x++)
	{
		hold.setX(x);
		this->_board[LINE_PAWN_WHITE][x] = new Pawn(hold, true);
	}

}

void Board::setUpper()
{
	buildUpper(true, LINE_KING_WHITE);
	buildUpper(false, LINE_KING_BLACK);
}

void Board::buildUpper(bool side, int line)
{
	Point hold(0, line);

	hold.setX(ROOK1);
	this->_board[line][ROOK1] = new Rook(hold, side);
	hold.setX(ROOK2);
	this->_board[line][ROOK2] = new Rook(hold, side);
	hold.setX(KNIGHT1);
	this->_board[line][KNIGHT1] = new Knight(hold, side);
	hold.setX(KNIGHT2);
	this->_board[line][KNIGHT2] = new Knight(hold, side);
	hold.setX(BISHOP1);
	this->_board[line][BISHOP1] = new Bishop(hold, side);
	hold.setX(BISHOP2);
	this->_board[line][BISHOP2] = new Bishop(hold, side);
	hold.setX(QUEEN);
	this->_board[line][QUEEN] = new Queen(hold, side);
	hold.setX(KING);
	this->_board[line][KING] = new King(hold, side);
}

Board::~Board()
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j])
			{
				delete this->_board[i][j]; // if there's a piece there.
			}
		}
	}
}

char Board::tryMove(const Point& src, const Point& dst, bool turn)
{ 
	// FIX PAWN AND CHECK IF RUN THROUGH OTHER PIECES WHEN MOVED!!!!!!
	std::cout << src.getX() << ", " << src.getY() << std::endl;
	std::cout << dst.getX() << ", " << dst.getY() << std::endl;
	this->draw();
	this->_turn = turn; // current turn
	if (this->inRange(src) && this->inRange(dst))
	{
		throw MOVE_OUT;
	}

	ChessPiece* pSrc = this->getPiece(src);
	ChessPiece* pDst = this->getPiece(dst);
	// Let's start the checking!

	if (!pSrc)
	{// Empty source
		throw MOVE_ENEMY;
	}
	if (src == dst)
	{
		throw MOVE_SAME;
	}
	if (pSrc->getSide() != this->_turn)
	{
		throw MOVE_ENEMY;
	}
	if (pDst && pDst->getSide() == this->_turn)
	{
		throw MOVE_ALLY;
	}// All good now let's check if he can actually do it.

	if (!pSrc->boardCheckMove(dst, this->_board))
	{
		throw MOVE_CANT;
	}// HE CAN DO IT!!!

	this->eatMove(src, dst); // Checks there if by that move he checks himself, and then finally moves

	this->draw();

	if (this->checkWinnable(!this->_turn))
	{
		return CHECK_MOVE;
	}
	return OK_MOVE;
	// return MOVE_CHECK_MATE
}

void Board::eatMove(const Point& src, const Point& dst)
{
	ChessPiece* travelTo = this->_board[dst.getY()][dst.getX()];
	ChessPiece* from = this->_board[src.getY()][src.getX()];

	this->_board[src.getY()][src.getX()] = nullptr;
	this->_board[dst.getY()][dst.getX()] = from;

	if (this->selfCheck())
	{
		this->_board[src.getY()][src.getX()] = from;
		this->_board[dst.getY()][dst.getX()] = travelTo;
		throw MOVE_SELF_CHECK;
	}

	if (travelTo) // there's a piece there, let me eat.
	{
		delete travelTo;
		std::cout << "Nam nam nam..." << std::endl;
	}
}

void Board::draw()
{
	char print;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			print = '#';
			if (this->_board[i][j])
			{
				print = this->_board[i][j]->getType();
			}
			std::cout << print << " ";

		}
		std::cout << std::endl;
	}
}

ChessPiece* Board::getPiece(const Point& place)
{
	return this->_board[place.getY()][place.getX()];
}

bool Board::checkWinnable(const bool turn)
{
	char kingKind = turn ? 'K' : 'k'; // White king | Black king.
	Point pKing(0, 0);

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j] && this->_board[i][j]->getType() == kingKind)
			{
				pKing.setX(j);
				pKing.setY(i);
				break;
			}
		}
	}

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j] && this->_board[i][j]->getSide() != turn)
			{
				if (this->_board[i][j]->justChecking(pKing, this->_board))
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool Board::selfCheck()
{
	return this->checkWinnable(this->_turn);
}

bool Board::checkMate()
{
	return false;
}

bool Board::inRange(const Point& p)
{
	return (p.getX() < 0 && p.getX() > BOARD_SIZE) || (p.getY() < 0 && p.getY() > BOARD_SIZE);
}