#pragma once
#include "AllPieces.h"
#include "Point.h"
#include <iostream>

#define BOARD_SIZE 8
#define LINE_KING_WHITE 7
#define LINE_PAWN_WHITE 6
#define LINE_KING_BLACK 0
#define LINE_PAWN_BLACK 1
#define MIN_COLLISION 2

enum moves { MOVE_GOOD, MOVE_CHECK, MOVE_ENEMY, MOVE_ALLY, MOVE_SELF_CHECK, MOVE_OUT, MOVE_CANT, MOVE_SAME, MOVE_CHECK_MATE };
enum pos {ROOK1, KNIGHT1, BISHOP1, KING, QUEEN, BISHOP2, KNIGHT2, ROOK2};
#define OK_MOVE '0'
#define CHECK_MOVE '1'

class Board
{
public:
	Board();
	~Board();

	char tryMove(const Point& src, const Point& dst, bool turn);
	ChessPiece* getPiece(const Point& place);

	bool checkWinnable(const bool turn);
	bool checkMate();
	bool selfCheck();

	void draw();

private:
	ChessPiece* _board[BOARD_SIZE][BOARD_SIZE];
	bool _turn; // White : BLack | True : Flase

	void startingSetup();
	void resetBoard();
	void setPawn();
	void setUpper();
	void buildUpper(bool side, int line);
	void eatMove(const Point& src, const Point& dst);

	bool inRange(const Point& p);
};