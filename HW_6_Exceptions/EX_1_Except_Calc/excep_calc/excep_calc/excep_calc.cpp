#include <iostream>

#define ERROR_CANNOT "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
#define ERROR_ZERO "Cannot cannot below 0 powa"

int add(int a, int b) {
	if (a + b == 8200)
	{
		throw(std::string(ERROR_CANNOT));
	}
	return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < abs(b); i++) {
    sum = add(sum, a * (abs(b) / b));
	if (sum == 8200)
	{
		throw(std::string(ERROR_CANNOT));
	}
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;

  if (b < 0)
  {
	  throw(std::string(ERROR_ZERO));
  }

  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
	if (exponent == 8200)
	{
		throw(std::string(ERROR_CANNOT));
	}
  };
  return exponent;
}

int main(void) {

	try
	{
		std::cout << pow(-5, 5) << std::endl;
	}
	catch (const std::string errorString)
	{
		std::cerr << "ERROR: " << errorString.c_str() << std::endl;
	}

	system("pause");
}