#pragma once
#include "shape.h"
#include <iostream>

class quadrilateral :public Shape {
public:
	virtual void draw();
	virtual double CalArea();

	quadrilateral();
	quadrilateral(std::string, std::string, int, int);

	int getHeight();
	int getWidth();

	void setHeight(int h);
	void setWidth(int w);

	double CalPerimater();
protected:
	int _width;
	int _height;
};