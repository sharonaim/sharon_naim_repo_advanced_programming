#pragma once
#include "shape.h"
#include "MathUtils.h"

class pentagon : public Shape
{
public:
	virtual void draw();
	virtual double CalArea();

	pentagon();
	pentagon(std::string, std::string, int);

	int getSide();

	void setSide(int side);

	double CalPerimater();
private: 
	int _side;
};