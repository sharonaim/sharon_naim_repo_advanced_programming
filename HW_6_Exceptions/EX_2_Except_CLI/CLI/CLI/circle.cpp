#include "circle.h"

Circle::Circle() : Shape("", "")
{
	this->_radius = 0;
}

Circle::Circle(std::string nam, std::string col, double rad) : Shape(col, nam) 
{
	if (rad < 0)
	{
		throw shapeException();
	}
	this->_radius = rad;
}

Circle::~Circle()
{
}

void Circle::draw()
{
	std::cout << std::endl 
		<< "Color is "<< this->_color << std::endl 
		<< "Name is " << this->_name << std::endl
		<< "Radius is " << this->_radius << std::endl
		<< "Circumference: " << this->CalCircumference() << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

void Circle::setRad(double rad) 
{
	if (rad < 0)
	{
		throw shapeException();
	}
	this->_radius = rad;
}

double Circle::CalArea() 
{
	double area = PI * this->_radius * this->_radius;
	
	return area;
	
}

double Circle:: getRad() 
{
	return this->_radius;
}

double Circle::CalCircumference()
{
	if (this->_radius <= 0)
	{
		throw shapeException();
	}
	return 2 * PI * this->_radius;
}