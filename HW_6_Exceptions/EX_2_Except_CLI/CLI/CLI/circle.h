#pragma once
#include "shape.h"
#include <iostream>

#define PI 3.14159265359

class Circle: public Shape {

public:
	Circle();
	Circle(std::string nam, std::string col, double rad);
	~Circle();

	virtual void draw();
	virtual double CalArea();

	double CalCircumference();
	double getRad();

	void setRad(double rad);
private:
	double _radius;
};