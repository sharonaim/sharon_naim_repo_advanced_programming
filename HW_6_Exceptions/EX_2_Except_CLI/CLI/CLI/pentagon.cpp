#include "pentagon.h"

void pentagon::draw()
{
	std::cout << std::endl
		<< "Color is " << this->_color << std::endl
		<< "Name is " << this->_name << std::endl
		<< "Sides are " << this->_side << std::endl
		<< "Perimeter: " << this->CalPerimater() << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->_side);
}

pentagon::pentagon() : Shape("", "")
{
	this->_side = 0;
}

pentagon::pentagon(std::string nam, std::string col, int side) : Shape(nam, col)
{
	if (side <= 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

int pentagon::getSide()
{
	return this->_side;
}

void pentagon::setSide(int side)
{
	if (side <= 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

double pentagon::CalPerimater()
{
	return 5 * this->_side;
}