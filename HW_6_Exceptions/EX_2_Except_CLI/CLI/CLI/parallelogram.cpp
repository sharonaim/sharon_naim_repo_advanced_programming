#include "parallelogram.h"

parallelogram::parallelogram() : quadrilateral()
{
	this->_angle = 0;
	this->_angle2 = 180;
}

parallelogram::parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2) : quadrilateral(col, nam, h, w) 
{
	if (ang + ang2 != 180 || ang < 0 || ang2 < 0)
	{
		throw shapeException();
	}
	this->_angle = ang;
	this->_angle2 = ang2;
}

void parallelogram::draw()
{
	std::cout << std::endl
		<< "Color is " << this->_color << std::endl
		<< "Name is " << this->_name << std::endl
		<< "Height is " << this->_height << std::endl
		<< "Width is " << this->_width << std::endl
		<< "Angle 1: " << this->_angle << std::endl
		<< "Angle 2: " << this->_angle2 << std::endl
		<< "Perimeter: " << this->CalPerimater() << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double parallelogram::CalArea()
{
	int h = sin(this->_angle) * this->_height; // calculating real height;
	return h * this->_width;
}

void parallelogram::setAngle(double ang, double ang2)
{
	if (ang + ang2 != 180 || ang < 0 || ang2 < 0)
	{
		throw shapeException();
	}
	this->_angle = ang;
	this->_angle2 = ang2;
}

double parallelogram::getAngle() 
{
	return this->_angle;
}

double parallelogram::getAngle2() 
{
	return this->_angle2;
}