#pragma once
#include "quadrilateral.h"
#include <iostream>

class parallelogram : public quadrilateral 
{
public:
	parallelogram();
	parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2);
	virtual void draw();
	virtual double CalArea();

	void setAngle(double, double);
	double getAngle2();
	double getAngle();

private:
	double _angle;
	double _angle2;
};