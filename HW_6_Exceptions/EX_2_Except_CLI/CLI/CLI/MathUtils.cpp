#include "MathUtils.h"

double MathUtils::CalPentagonArea(double side)
{
	return 5 * pow(side, 2) / (4 * tan(36)); // formula for regular pentagon area
}

double MathUtils::CalHexagonArea(double side)
{
	return 3 * sqrt(3) * pow(side, 2) / 2; // formula for regular hexagon area
}