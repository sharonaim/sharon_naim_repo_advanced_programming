#pragma once
#include "shape.h"
#include "MathUtils.h"

class hexagon : public Shape
{
public:
	virtual void draw();
	virtual double CalArea();

	hexagon();
	hexagon(std::string, std::string, int);

	int getSide();

	void setSide(int side);

	double CalPerimater();
private:
	int _side;
};

