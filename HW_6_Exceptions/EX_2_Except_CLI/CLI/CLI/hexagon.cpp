#include "hexagon.h"

void hexagon::draw()
{
	std::cout << std::endl
		<< "Color is " << this->_color << std::endl
		<< "Name is " << this->_name << std::endl
		<< "Sides are " << this->_side << std::endl
		<< "Perimeter: " << this->CalPerimater() << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(this->_side);
}

hexagon::hexagon() : Shape("", "")
{
	this->_side = 0;
}

hexagon::hexagon(std::string nam, std::string col, int side) : Shape(nam, col)
{
	if (side <= 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

int hexagon::getSide()
{
	return this->_side;
}

void hexagon::setSide(int side)
{
	if (side <= 0)
	{
		throw shapeException();
	}
	this->_side = side;
}

double hexagon::CalPerimater()
{
	return 6 * this->_side;
}