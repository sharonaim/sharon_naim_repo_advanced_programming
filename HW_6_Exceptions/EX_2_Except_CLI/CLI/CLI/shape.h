#pragma once
#include <string>
#include <iostream>
#include "shapeexception.h"

class Shape
{
public:
	virtual void draw() = 0; //DEFINE FOR ALL
	virtual double CalArea() = 0;//DEFINE FOR ALL

	Shape(std::string, std::string);
	~Shape();

	void setName(std::string);
	void setColor(std::string);

	std::string getName();
	std::string getColor();
	static int getCount();

private:
	static int _count;

protected:
	std::string _name;
	std::string _color;
};