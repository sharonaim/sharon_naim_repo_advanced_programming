#pragma once
#include "quadrilateral.h"
#include <iostream>

class rectangle : public quadrilateral {
public:
	rectangle();
	rectangle(std::string, std::string, int, int);

	virtual void draw();
	// virtual double CalArea(); in quadrilateral

	bool isSquare();
	const char* getIsSquare();
};