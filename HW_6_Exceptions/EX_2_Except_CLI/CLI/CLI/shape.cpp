#include "shape.h"

int Shape::_count = 0;

 Shape::Shape(std::string nam, std::string col)
{
	this->_name = nam;
	this->_color = col;
	this->_count++;
}

int Shape::getCount() 
{
	return _count;
}

Shape::~Shape()
{
	_count--;
}

void Shape::setName(std::string nam)
{
	this->_name = nam;
}

void Shape::setColor(std::string col)
{
	this->_color = col;
}

std::string Shape::getName()
{
	if (this->_name == "")
	{
		throw shapeException();
	}
	return this->_name;
}

std::string Shape::getColor() 
{
	if (this->_color == "")
	{
		throw shapeException();
	}
	return this->_color;
}