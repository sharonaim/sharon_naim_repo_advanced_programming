#pragma once
#include <math.h>

class MathUtils
{
public:
	static double CalPentagonArea(double side);
	static double CalHexagonArea(double side);
};

