#include "shape.h"
#include "rectangle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

rectangle::rectangle() : quadrilateral()
{
}

rectangle::rectangle(std::string nam, std::string col, int w, int h): quadrilateral(nam,col,w, h)
{
}

void rectangle::draw()
{

	std::cout << std::endl
		<< "Color is " << this->_color << std::endl
		<< "Name is " << this->_name << std::endl
		<< "Height is " << this->_height << std::endl
		<< "Width is " << this->_width << std::endl
		<< "Perimeter: " << this->CalPerimater() << std::endl
		<< "Area: " << this->CalArea() << std::endl
	    << "Square? " << this->getIsSquare() << std::endl;
}

const char* rectangle::getIsSquare()
{
	if (this->isSquare())
	{
		return "True";
	}
	else
	{
		return "False";
	}
}

bool rectangle::isSquare() 
{
	return this->_height == this->_width;
}