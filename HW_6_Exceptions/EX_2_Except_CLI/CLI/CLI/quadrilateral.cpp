#include "quadrilateral.h"

quadrilateral::quadrilateral() : Shape("", "")
{
	this->_height = 0;
	this->_width = 0;
}

quadrilateral::quadrilateral(std::string nam, std::string col, int h, int w) : Shape(nam, col) 
{
	if (h < 0 || w < 0)
	{
		throw shapeException();
	}
	this->_height = h;
	this->_width = w;
}

void quadrilateral::draw()
{
	std::cout << std::endl
		<< "Color is " << this->_color << std::endl
		<< "Name is " << this->_name << std::endl
		<< "Height is " << this->_height << std::endl
		<< "Width is " << this->_width << std::endl
		<< "Perimeter: " << this->CalPerimater() << std::endl
		<< "Area: " << this->CalArea() << std::endl;
}

double quadrilateral::CalArea()
{
	if (this->_width < 0 || this->_height < 0)
	{
		throw shapeException();
	}
	return this->_width * this->_height; //RECTANGLE 
}

void quadrilateral::setHeight(int h) 
{
	if (h < 0)
	{
		throw shapeException();
	}
	this->_height = h;
}

void quadrilateral::setWidth(int w) 
{
	if (w < 0)
	{
		throw shapeException();
	}
	this->_width = w;
}

double quadrilateral::CalPerimater() 
{
	if (this->_width < 0 || this->_height < 0)
	{
		throw shapeException();
	}
	return 2 * (this->_height + this->_width);
}

int quadrilateral::getHeight() 
{
	return this->_height;
}

int quadrilateral::getWidth() 
{
	return this->_width;
}