#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "pentagon.h"
#include "hexagon.h"

#define CIRCLE 'c'
#define QUAD 'q'
#define RECT 'r'
#define PARAL 'p'
#define TAGO 't'
#define HEXA 'h'
#define CHAR_LEN 1

void getSimple(Shape& s, std::string& nam, std::string& col);
void checkInput();

int main()
{
	std::string nam, col; 
	double rad, ang, ang2; 
	int height, width;

	Circle circ;
	quadrilateral quad;
	rectangle rec;
	parallelogram para;
	pentagon pent;
	hexagon hexa;

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent;
	Shape *ptrhexa = &hexa;

	std::string buffer;
	char shapetype;
	char x = 'y';
	
	std::cout << "Enter information for your objects" << std::endl;
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, t = pentagon, h = hexagon" << std::endl;
		std::cin >> buffer;

		if (buffer.size() == CHAR_LEN)
		{
			shapetype = buffer[0];
		}
		else
		{
			std::cout << "Warning - Don't try to build more than one shape at once" << std::endl;
			shapetype = '0';
		}
try
		{
			switch (shapetype) {
			case CIRCLE:
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				
				checkInput();

				getSimple(circ, nam, col);
				circ.setRad(rad);

				ptrcirc->draw();
				break;
			case QUAD:
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				
				checkInput();

				getSimple(quad, nam, col);
				quad.setHeight(height);
				quad.setWidth(width);

				ptrquad->draw();
				break;
			case RECT:
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				
				checkInput();

				getSimple(rec, nam, col);
				rec.setHeight(height);
				rec.setWidth(width);

				ptrrec->draw();
				break;
			case PARAL:
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				
				checkInput();

				getSimple(para, nam, col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);

				ptrpara->draw();
				break;
			case TAGO:
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> height;

				checkInput();

				getSimple(pent, nam, col);
				pent.setSide(height);

				ptrpent->draw();
				break;
			case HEXA:
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> height;

				checkInput();

				getSimple(hexa, nam, col);
				hexa.setSide(height);

				ptrhexa->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}

			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get(); // clear buffer
			x = std::cin.get();
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception& e)
		{			
			std::cout << e.what() << std::endl;
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;
	
}

void getSimple(Shape& s, std::string& nam, std::string& col)
{
	s.setName(nam);
	s.setColor(col);
}

void checkInput()
{
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		throw InputException();
	}
}